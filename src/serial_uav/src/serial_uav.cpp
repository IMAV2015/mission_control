#include "ros/ros.h"
#include "rosMessage.h"
#include "serialFunc.h"

#include <sstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <unistd.h>
#include <fstream>


using namespace std;

string serial_port = (string)"/dev/ttyUSB0";


void readPara(){
	std::ifstream infile("parameters.txt");

	std::string line;
	while(std::getline(infile, line)){
		std::istringstream iss(line);
		std::string name;
		std::string value;
		if(!(iss>>name>>value)){
			continue;
		}

		if(name.c_str()[0] == '#'){
			//std::cout<<"detected"<<std::endl;
			continue;
		}
		else if(name.compare("serial_port")==0){
			serial_port = value;
		}

	}

}


int parseLine(char* line) {
	int i = strlen(line);
	while (*line < '0' || *line > '9')
		line++;
	line[i - 3] = '\0';
	i = atoi(line);
	return i;
}

int getValue() { //Note: this value is in KB!
	FILE* file = fopen("/proc/self/status", "r");
	int result = -1;
	char line[128];

	while (fgets(line, 128, file) != NULL) {
		if (strncmp(line, "VmRSS:", 6) == 0) {
			result = parseLine(line);
			break;
		}
	}
	fclose(file);
	return result;
}




int main(int argc, char **argv){
	cout<<"hello ros"<<endl;
	//initialize ros
	ros::init(argc, argv, "serial_uav_node");

	//ros node variables
	ros::NodeHandle rosNH;
	ros::Publisher serial2ros = rosNH.advertise<serial_uav::MSG_UAV_ROS>("serial2ros", 1000);
	
	//serialUAV serial((char*)"/dev/ttyUSB0");
	serialUAV serial((char*)serial_port.c_str());
	cout<<"open status: "<<serial.status_port<<endl;

	ros_serial ros_serial_sub(&serial);
	ros::Subscriber ros2serial = rosNH.subscribe("ros2serial", 1000, &ros_serial::ros2serial_callback, &ros_serial_sub );
	ros::AsyncSpinner spinner(2); // Use 2 threads
	spinner.start();


	//ROS Spin
	ros::Rate rosRate(30);
	


	// receive from uav
	while (1) {
		//clock_t t_start = clock();
		try{
			serial.readMsgFromSerial();
		}
		catch(int error){
			cout<<"error in reading msg from serial."<<endl;
		}
		//clock_t t_end = clock();
		//cout<<"time consumed: "<<(double)(t_end-t_start) / CLOCKS_PER_SEC * 1000<<endl<<"memory: "<<getValue()<<endl;


		if(serial.status_receive && serial.receive.cmd!=0){
			cout << endl << "received msg:" << std::dec << serial.receive.cmd << "  vector: ";
			serial.printVector(serial.receive.parameters);

			//define msg content
//			serial_uav::MSG_UAV_ROS msgFromUAV;
//			msgFromUAV.cmd = serial.receive.cmd;
//
//			if(serial.receive.parameters.size()>0)
//				msgFromUAV.parameters = convertVectorFloat2Double(serial.receive.parameters);
//			else
//				msgFromUAV.parameters.clear();
//
//			if (ros::ok()) {
//				serial2ros.publish(msgFromUAV);
//			}

			for (int i=serial.receive_history.size()-1;i>=0;--i){

				//define msg content
				serial_uav::MSG_UAV_ROS msgFromUAV;
				msgFromUAV.cmd = serial.receive_history.at(i).cmd;

				if(serial.receive_history.at(i).parameters.size()>0)
					msgFromUAV.parameters = convertVectorFloat2Double(serial.receive_history.at(i).parameters);
				else
					msgFromUAV.parameters.clear();

				if (ros::ok()) {
					serial2ros.publish(msgFromUAV);
				}

			}

		}


		rosRate.sleep();
	}


	return 0;

}



