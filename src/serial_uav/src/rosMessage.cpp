#include "rosMessage.h"

using namespace std;


void ros_serial::ros2serial_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg){
	//cout<<"ros message received callback"<<endl;


	// send to uav
	serial->send.cmd = ros_msg->cmd;
	if(ros_msg->parameters.size()>0)
		serial->send.parameters = convertVectorDouble2Float(ros_msg->parameters);
	else
		serial->send.parameters.clear();

	cout<<endl;
	cout<<"send msg: "<<std::dec<<serial->send.cmd<<"  ";
	serial->printVector(serial->send.parameters);

   	if(serial->writeMsg2Serial()){
   		//cout<<"Write to UAV successfully."<<endl;
   	}
   	else{
   		cout<<"Fail writing to UAV."<<endl;
   	}
   	

}

vector<double> convertVectorFloat2Double(const vector<float> &src){
	vector<double> dst(src.begin(), src.end());
	return dst;
}

vector<float> convertVectorDouble2Float(const vector<double> &src){
	vector<float> dst(src.begin(), src.end());
	return dst;
}
