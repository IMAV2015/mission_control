#ifndef SERIALFUNC_H
#define SERIALFUNC_H

//#define UAV_CPP

#ifdef UAV_CPP

//#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <iterator>
//#include <algorithm>
#include <list>
#include <cstring>


// CRC configuration -----------------------
typedef unsigned short  crc;
#define CRC_CCITT

#define CRC_NAME			"CRC-CCITT"
#define POLYNOMIAL			0x1021
#define INITIAL_REMAINDER	0xFFFF
#define FINAL_XOR_VALUE		0x0000
#define REFLECT_DATA		FALSE
#define REFLECT_REMAINDER	FALSE
#define CHECK_VALUE			0x29B1

#define WIDTH    (8 * sizeof(crc))
#define TOPBIT   (1 << (WIDTH - 1))

// CRC configuration -----------------------


#define BufferSize 4096



using namespace std;

struct pack{
	int cmd;
	vector<float> paras;
	vector<float> ref_pos;
	vector<float> ref_vel;
	vector<float> ref_acc;
	vector<float> mea_pos;
	vector<float> mea_vel;
	vector<float> mea_acc;
};

struct msg_serial{
	int cmd;
	vector<float> parameters;
};

class serialUAV{
public:
	bool status_send;
	bool status_receive;
	bool status_port;
	msg_serial receive;
	msg_serial send;



private:
	int USB;
	string port;
	int init_counter;

	unsigned char Header_1;
	unsigned char Header_2;
	unsigned char Header_3;

	unsigned char Ending_1;
	unsigned char Ending_2;
	unsigned char Ending_3;


	unsigned char readBuff[BufferSize];
	// for debug
	//unsigned char readBuff[36] = {1,2,15,240,255,1,2,3,4,5,6,7,8,9,10,114,44,254,240,16,1,2,15,240,255,2,1,9,9,9,9,1,1,254,240,1};

	list<unsigned char> serialContainer;  // contains the data from serial port. the beginning of this list is always the header.
	vector<unsigned char> rawData;  // contains the uchar of a complete pack.


public:
	serialUAV(string port_in){
		crcInit();

		port = port_in;
		init_counter = 0;

		status_port = initialSerial();

		Header_1 = 15;
		Header_2 = 240;
		Header_3 = 255;

		Ending_1 = 254;
		Ending_2 = 240;
		Ending_3 = 16;

		status_send = false;
		status_receive = false;
		send.cmd = 0;
		receive.cmd = 0;



	}

	serialUAV(){};

	~serialUAV(){

	}

	bool initialSerial();

	int getWord(const vector<unsigned char> &rawData, int index);

	void putWord(unsigned char *pointer, int data);

	float getFloat(const vector<unsigned char> &rawData, int index);

	void putFloat(unsigned char *pointer, float data);


	void readMsgFromSerial();

	void processRawData();

	void printVector(vector<float> &data){
		for(int i=0;i<data.size();++i){
			cout<<data.at(i)<<", ";
		}
		cout<<endl;
	}

	void dispRawData(){
	    cout<<endl<<"Raw Data"<<endl;
	    for(int i=0;i<rawData.size();++i){
	        cout<<(int)(rawData.at(i))<<", ";
	    }
	    cout<<endl;
	}


	int write2Serial(unsigned char *dataWrite, int writeLength);

	int writeMsg2Serial();

private:
	int cutTillHeader();
	int findEnding(std::list<unsigned char>::iterator &iter);


	// for crc
	crc  crcTable[256];

	void  crcInit(void);
	crc   crcSlow(unsigned char const message[], int nBytes);
	crc   crcFast(unsigned char const message[], int nBytes);

};


#else


#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <vector>



// CRC configuration -----------------------
typedef unsigned short  crc;
#define CRC_CCITT

#define CRC_NAME			"CRC-CCITT"
#define POLYNOMIAL			0x1021
#define INITIAL_REMAINDER	0xFFFF
#define FINAL_XOR_VALUE		0x0000
#define REFLECT_DATA		FALSE
#define REFLECT_REMAINDER	FALSE
#define CHECK_VALUE			0x29B1

#define WIDTH    (8 * sizeof(crc))
#define TOPBIT   (1 << (WIDTH - 1))

// CRC configuration -----------------------


#define BufferSize 512
#define ContentLength 256



using namespace std;


struct msg_serial{
	int cmd;
	vector<float> parameters;
};

class serialUAV{
public:
	bool status_send;
	bool status_receive;
	bool status_port;
	msg_serial receive;
	msg_serial send;

	vector<msg_serial> receive_history;

private:
	int USB;
	char* port;
	int init_counter;

	unsigned char Header_1;
	unsigned char Header_2;
	unsigned char Header_3;

	unsigned char Ending_1;
	unsigned char Ending_2;
	unsigned char Ending_3;


	unsigned char readBuff[BufferSize];
	// for debug
	//unsigned char readBuff[18] = {254,240,16,15,240,255,1,2,3,4,5,6,7,8,9,10,114,44};

	unsigned char serialContainer[BufferSize];  // contains the data from serial port. the beginning of this list is always the header.
	int serialContainerSize;
	unsigned char rawData[ContentLength];  // contains the uchar of a complete pack.
	int rawDataSize;

	unsigned char dataWrite[ContentLength];


public:
	serialUAV(char* port_in){
		crcInit();


		port = port_in;
		init_counter = 0;

		status_port = initialSerial();

		Header_1 = 15;
		Header_2 = 240;
		Header_3 = 255;

		Ending_1 = 254;
		Ending_2 = 240;
		Ending_3 = 16;

		status_send = false;
		status_receive = true;
		send.cmd = 0;
		receive.cmd = 0;

		serialContainerSize = 0;
		rawDataSize = 0;

	}

	~serialUAV(){
	}

	bool initialSerial();

	int getWord(const unsigned char *rawData, int index);

	void putWord(unsigned char *pointer, int data);

	float getFloat(const unsigned char *rawData, int index);

	void putFloat(unsigned char *pointer, float data);


	void readMsgFromSerial();

	void processRawData();

	void printVector(vector<float>& data){
		for(int i=0;i<data.size();++i){
			printf("%f, ", (float)data.at(i));
		}
		printf("\n");
	}

	void dispRawData(){
	    printf("Raw data:\n");
	    for(int i=0;i<rawDataSize;++i){
	        printf("%d, ", (int)(rawData[i]));
	    }
	    printf("\n");
	}


	int write2Serial(const unsigned char *dataWrite, int writeLength);

	int writeMsg2Serial();

private:
	int cutTillHeader();
	int findEnding(int &index);


	// for crc
	crc  crcTable[256];

	void  crcInit(void);
	crc   crcFast(unsigned char const message[], int nBytes);

};


#endif



#endif
