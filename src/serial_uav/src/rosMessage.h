#ifndef ROSMESSAGE_H
#define ROSMESSAGE_H

#include "serialFunc.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "serial_uav/MSG_UAV_ROS.h"

using namespace std;



class ros_serial{
public:
	serial_uav::MSG_UAV_ROS msgToUAV;
private:
	serialUAV *serial;


public:
	ros_serial(serialUAV* serial_in){
		serial = serial_in;
	}

	~ros_serial(){

	}

	void ros2serial_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg);

};


vector<double> convertVectorFloat2Double(const vector<float> &src);

vector<float> convertVectorDouble2Float(const vector<double> &src);


#endif
