#include "visual_guide.h"
#include "camera.h"
void pond_detector::HSVParameterRead(const char* s)
{
        int n,m,o,p,q,r,i=1; /*,lines;*/
        ifstream configFile;
        configFile.open(s);

        if (!configFile.good()){
            perror("[kevin] configuration file: ");
            exit(0);
        }

        while(configFile>>n>>m>>o>>p>>q>>r){
 		_color_thresh_lower_water = Scalar(n,m,o);
 		_color_thresh_upper_water = Scalar(p,q,r);
        i++;
        }
        // cout << "_color_thresh_lower_water " << _color_thresh_lower_water << endl;
        // cout << "_color_thresh_upper_water " << _color_thresh_upper_water << endl;        
        // cout << "no. of lines" << i << endl;
}

void pond_detector::detect()
{
  	undistort(img_original, img_undistorted, intrinsic, distCoeffs);	
	cvtColor(img_undistorted, img_HSV, CV_BGR2HSV); // BGR instead of RGB
	inRange(img_HSV,_color_thresh_lower_water,_color_thresh_upper_water,img_binary);
#if VDISPLAY	
		imshow("img_binary",img_binary);
		waitKey(0);	
#endif	
	vector<vector<Point> > contourAll;
	vector<Vec4i> hierarchy;	

	findContours(img_binary, contourAll, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	vector<RotatedRect> allEllipse;
	img_original.copyTo(img_publish); 
	int ellipse_num = kev_DetectEllipseFromContour(contourAll, hierarchy, allEllipse, img_publish);
	fcn_PlotEllipse(img_publish, allEllipse, ellipse_num, Scalar(255,255,0));	
#if VDISPLAY	
		imshow("img_publish",img_publish);
		waitKey(0);	
#endif
	if (ellipse_num > 0 && _index_best_fit >= 0)
	{
		//cout << "_index_best_fit " << _index_best_fit << endl;
		ellipse( img_publish, allEllipse[_index_best_fit], Scalar(255,255,255), 5, 8 );
	}

	if (ellipse_num>=1)
	{
		fcn_OneCirlcePoseEstimation_4Points(allEllipse[_index_best_fit],_t);
		_Target = allEllipse[_index_best_fit];
		cout << "Relative position" << _t << endl;

		flag = 1;

		char numStr[100]="";
		sprintf(numStr, "tvec=[%.1f, %.1f, %.1f]", _t.at<double>(0,0), _t.at<double>(1,0), _t.at<double>(2,0) );
		putText(img_publish, numStr, Point(0, 25),  FONT_HERSHEY_PLAIN, 2, Scalar(255,255,255), 2, 8, false);		
	}
	else
	{
		flag = 0;
	}	
#if VDISPLAY	
		imshow("img_publish",img_publish);
		waitKey(0);	
#endif			
}

int pond_detector::bucket_DetectEllipseFromContour(vector<vector<Point> > contourAll, vector<Vec4i> hierarchy, vector<RotatedRect>& minEllipse_output, Mat& drawing)
{
	// input: contourAll and hierarchy
	// output: all good ellipse, including the overlapped ones and half ones
	int i, j;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// remove false detection using various criteria
	int good_contour_num=0;
	vector<int> good_contour_index(contourAll.size()); // store the index of the good contours
	vector<int> good_contour_type(contourAll.size()); // 1 for single ellipse; 2 for two overlaped ellipse, 3, 4 ...

	vector<RotatedRect> good_contour_ellipse(contourAll.size());
	// moments
	Moments allMoments;
	double I1, I2, I3;
	double I1_ellipse=0.0063;
	double threshold_I1_ellipse=0.0003; // I1_ellipse-threshold_I1_ellipse < I1 < I1_ellipse+threshold_I1_ellipse
	double I2_ellipse=0;
	double threshold_I2_ellipse=0.0000001;
	double I3_ellipse=0;
	double threshold_I3_ellipse=0.000001; //0.0000005;
	double I1_ellipse_overlap=0.008;
	double threshold_I1_ellipse_overlap_upper=0.002;
	double threshold_I1_ellipse_overlap_lower=0.001;
	double I2_ellipse_overlap=0;
	double threshold_I2_ellipse_overlap=0.0000001;
	double I3_ellipse_overlap=0;
	double threshold_I3_ellipse_overlap=0.000001;
	//
	for(i = 0; i < contourAll.size(); i++ )
	{
//		cout << "Area1: " << contourAll[i].size() << endl;
//		cout << "Area2: " << contourArea(contourAll[i]) << endl;
		// (1) remove too small contourAll
		if( contourArea(contourAll[i]) > _threshold_remove_small_contour /*contourAll[i].size() > threshold_remove_small_contour*/ )
		{
			// (2) compute affine invariant moment of the contour
			allMoments=moments(contourAll[i], true); // true=binary image
			double mu00=allMoments.m00, mu30=allMoments.mu30, mu03=allMoments.mu03, mu12=allMoments.mu12, mu21=allMoments.mu21, mu11=allMoments.mu11, mu02=allMoments.mu02, mu20=allMoments.mu20;
			I1= ( mu20*mu02 - mu11*mu11 ) / pow(mu00, 4);
			I2 = (mu30*mu30*mu03*mu03 - 6*mu30*mu21*mu12*mu03 + 4*mu30*mu12*mu12*mu12 + 4*mu21*mu21*mu21*mu03 - 3*mu21*mu21*mu12*mu12) / pow(mu00,10);
			I3 = (mu20*(mu21*mu03-mu12*mu12) - mu11*(mu30*mu03-mu21*mu12) + mu02*(mu30*mu12-mu21*mu21)) / pow(mu00,7);
			// check if it is an ellipse
			if ( (abs(I1-I1_ellipse)<=threshold_I1_ellipse) && (abs(I2-I2_ellipse)<=threshold_I2_ellipse) && (abs(I3-I3_ellipse)<=threshold_I3_ellipse) )
			{
				// then it is an ellipse
				good_contour_type[good_contour_num]=1; // 1 stands for ellipse
				good_contour_index[good_contour_num]=i;
				good_contour_num++;
			}
			else if ( (I1<=I1_ellipse_overlap+threshold_I1_ellipse_overlap_upper) && (I1>=I1_ellipse_overlap-threshold_I1_ellipse_overlap_lower)
					&& (abs(I2-I2_ellipse_overlap)<=threshold_I2_ellipse_overlap) && (abs(I3-I3_ellipse_overlap)<=threshold_I3_ellipse_overlap) )
			{
				// check if it is a overlapped ellipse
				good_contour_type[good_contour_num]=-1; // -1 stands for non-ellipse, may be overlapped ellipses
				good_contour_index[good_contour_num]=i;
				good_contour_num++;
			}
			else
			{
				// check if it is a concave ellipse
				//vector<vector<Point>> hull(1, int(contourAll[i].size()));
				vector<vector<Point> > hull(1, vector<Point> (int(contourAll[i].size()), Point()));
				convexHull( Mat(contourAll[i]), hull[0] );
				// drawContours(drawing, hull, -1, Scalar(0,0,255), 1, 8, vector<Vec4i>(), 0, Point() );
				if (hull[0].size() >= _threshold_remove_small_contour/2)
				{
					RotatedRect ellipse_convexHull = fitEllipse( Mat(hull[0]) ); // note the return is a RotatedRect
					double good2bad_ratio = fcn_GetEllipseGoodPointRatio(contourAll[i], ellipse_convexHull, contourAll[i].size(), _threshold_algbraic_error); // judge how many points of the contour on the ellipse, at least more than a half should be
					if (good2bad_ratio > _threshold_good_point_ratio)
					{
						// then it is concave ellipses
						good_contour_type[good_contour_num]=0; // 0 stands for concave ellipse
						good_contour_index[good_contour_num]=i;
						good_contour_num++;
						//printf(">>>>A concave ellipse: goodPointRatio = %f\n", good2bad_ratio);
					}
				}////if (hull[0].size() >= 5)
			}
		}
	} //// for(i = 0; i < contourAll.size(); i++ )

//	// draw the good contour
//	for (i=0; i<good_contour_num; i++)
//	{
//	 drawContours(drawing, contourAll, good_contour_index[i], Scalar(0,255,0), 1, 8, hierarchy, 0, Point() );
//	}
//	imshow("contours", drawing);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// fit ellipse for each good contour
	vector<RotatedRect> minEllipse( 4*contourAll.size() ); // prepare enough room for ellipse
	vector<double> good_contour_error(4*contourAll.size()); //added by Kevin
	//minEllipse.reserve(4*contourAll.size()); // does NOT work
	int ellipse_num=0; // the number of all ellipse on the image
	for (i=0; i<good_contour_num; i++) // for each good contour
	{
		if (good_contour_type[i]==1) // normal ellipse
		{
			minEllipse[ellipse_num] = fitEllipse( Mat(contourAll[good_contour_index[i]]) ); // note the return is a RotatedRect
			if (max(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) / min(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) <_threshold_remove_thin_ellipse_ratio)
			{
				good_contour_error[i]= fcn_GetAlgError(contourAll[good_contour_index[i]], minEllipse[ellipse_num],contourAll[good_contour_index[i]].size());
				ellipse_num++;
			}
		}
		else if (good_contour_type[i]==0) // concave ellipse
		{
			//vector<vector<Point> > hull(1, int(contourAll[good_contour_index[i]].size()));
			vector<vector<Point> > hull(1, vector<Point> (int(contourAll[good_contour_index[i]].size()), Point()));
			convexHull( Mat(contourAll[good_contour_index[i]]), hull[0] );
			minEllipse[ellipse_num] = fitEllipse( Mat(hull[0]) );
			if (max(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) / min(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) <_threshold_remove_thin_ellipse_ratio)
			{
				good_contour_error[i]= fcn_GetAlgError(contourAll[good_contour_index[i]], minEllipse[ellipse_num],contourAll[good_contour_index[i]].size());
				ellipse_num++;
			}
		}
//		else if (good_contour_type[i]==-1) // overlapped ellipse
//		{
//			vector<RotatedRect> overlapEllipses; //
//			int overlap_ellipse_num = fcn_partitionOverlappedEllipse(contourAll[good_contour_index[i]], overlapEllipses);
//			for (j=0; j<overlap_ellipse_num; j++)
//			{
//				minEllipse[ellipse_num]=overlapEllipses[j];
//				ellipse_num++;
//			}
//			//printf(">>>>An overlapped  ellipse: contain %d ellipses\n", overlap_ellipse_num);
//		}
	} //// for (i=0; i<good_contour_num; i++)
	//

	// Kevin Added: sort ellipse that have lowest algError
//	if(good_contour_num>0)
//	{
//	for (int i=0 ; i < good_contour_num ; i++)
//	{
//		good_contour_ellipse[i]= fitEllipse( Mat(contourAll[good_contour_index[i]]) );
//		good_contour_error[i]= fcn_GetAlgError(contourAll[good_contour_index[i]], good_contour_ellipse[i],contourAll[good_contour_index[i]].size());
//
//	}
//	}

//	cout <<"Total Good contours: " << ellipse_num << endl;
//	for (int i = 0 ; i < good_contour_index.size(); i++)
//	{
//		cout << "index: " << good_contour_index[i] << endl;
//		cout << "contour error: " << good_contour_error[i] << endl;
//		cout << "number of pts: " << contourAll[good_contour_index[i]].size() << endl;
//	}

int index_lowest_error = distance(good_contour_error.begin(), min_element (good_contour_error.begin(),good_contour_error.end()));
//if (index_lowest_error>0)
//{
//_index_best_fit = index_lowest_error - 1;
//}
//else
//	_index_best_fit = 0;
//	cout << "index of min error element: " << index_lowest_error-1 <<endl;
	_index_best_fit_bucket = index_lowest_error-1;
//	cout << "contour index: " << good_contour_index[index_lowest_error-1] << endl;
//
//	ellipse_best_fit = good_contour_ellipse[_index_best_fit];

	minEllipse_output=minEllipse; // return the pointer
	return ellipse_num; // return the ellipse number
}


int pond_detector::kev_DetectEllipseFromContour(vector<vector<Point> > contourAll, vector<Vec4i> hierarchy, vector<RotatedRect>& minEllipse_output, Mat& drawing)
{
	// input: contourAll and hierarchy
	// output: all good ellipse, including the overlapped ones and half ones
	int i, j;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// remove false detection using various criteria
	int good_contour_num=0;
	vector<int> good_contour_index(contourAll.size()); // store the index of the good contours
	vector<int> good_contour_type(contourAll.size()); // 1 for single ellipse; 2 for two overlaped ellipse, 3, 4 ...

	vector<RotatedRect> good_contour_ellipse(contourAll.size());
	// moments
	Moments allMoments;
	double I1, I2, I3;
	double I1_ellipse=0.0063;
	double threshold_I1_ellipse=0.0003; // I1_ellipse-threshold_I1_ellipse < I1 < I1_ellipse+threshold_I1_ellipse
	double I2_ellipse=0;
	double threshold_I2_ellipse=0.0000001;
	double I3_ellipse=0;
	double threshold_I3_ellipse=0.000001; //0.0000005;
	double I1_ellipse_overlap=0.008;
	double threshold_I1_ellipse_overlap_upper=0.002;
	double threshold_I1_ellipse_overlap_lower=0.001;
	double I2_ellipse_overlap=0;
	double threshold_I2_ellipse_overlap=0.0000001;
	double I3_ellipse_overlap=0;
	double threshold_I3_ellipse_overlap=0.000001;
	//
	for(i = 0; i < contourAll.size(); i++ )
	{
//		cout << "Area1: " << contourAll[i].size() << endl;
//		cout << "Area2: " << contourArea(contourAll[i]) << endl;
		// (1) remove too small contourAll
		if( contourArea(contourAll[i]) > _threshold_remove_small_contour /*contourAll[i].size() > threshold_remove_small_contour*/ )
		{
			// (2) compute affine invariant moment of the contour
			allMoments=moments(contourAll[i], true); // true=binary image
			double mu00=allMoments.m00, mu30=allMoments.mu30, mu03=allMoments.mu03, mu12=allMoments.mu12, mu21=allMoments.mu21, mu11=allMoments.mu11, mu02=allMoments.mu02, mu20=allMoments.mu20;
			I1= ( mu20*mu02 - mu11*mu11 ) / pow(mu00, 4);
			I2 = (mu30*mu30*mu03*mu03 - 6*mu30*mu21*mu12*mu03 + 4*mu30*mu12*mu12*mu12 + 4*mu21*mu21*mu21*mu03 - 3*mu21*mu21*mu12*mu12) / pow(mu00,10);
			I3 = (mu20*(mu21*mu03-mu12*mu12) - mu11*(mu30*mu03-mu21*mu12) + mu02*(mu30*mu12-mu21*mu21)) / pow(mu00,7);
			// check if it is an ellipse
			if ( (abs(I1-I1_ellipse)<=threshold_I1_ellipse) && (abs(I2-I2_ellipse)<=threshold_I2_ellipse) && (abs(I3-I3_ellipse)<=threshold_I3_ellipse) )
			{
				// then it is an ellipse
				good_contour_type[good_contour_num]=1; // 1 stands for ellipse
				good_contour_index[good_contour_num]=i;
				good_contour_num++;
			}
			else if ( (I1<=I1_ellipse_overlap+threshold_I1_ellipse_overlap_upper) && (I1>=I1_ellipse_overlap-threshold_I1_ellipse_overlap_lower)
					&& (abs(I2-I2_ellipse_overlap)<=threshold_I2_ellipse_overlap) && (abs(I3-I3_ellipse_overlap)<=threshold_I3_ellipse_overlap) )
			{
				// check if it is a overlapped ellipse
				good_contour_type[good_contour_num]=-1; // -1 stands for non-ellipse, may be overlapped ellipses
				good_contour_index[good_contour_num]=i;
				good_contour_num++;
			}
			else
			{
				// check if it is a concave ellipse
				//vector<vector<Point>> hull(1, int(contourAll[i].size()));
				vector<vector<Point> > hull(1, vector<Point> (int(contourAll[i].size()), Point()));
				convexHull( Mat(contourAll[i]), hull[0] );
				// drawContours(drawing, hull, -1, Scalar(0,0,255), 1, 8, vector<Vec4i>(), 0, Point() );
				if (hull[0].size() >= _threshold_remove_small_contour/2)
				{
					RotatedRect ellipse_convexHull = fitEllipse( Mat(hull[0]) ); // note the return is a RotatedRect
					double good2bad_ratio = fcn_GetEllipseGoodPointRatio(contourAll[i], ellipse_convexHull, contourAll[i].size(), _threshold_algbraic_error); // judge how many points of the contour on the ellipse, at least more than a half should be
					if (good2bad_ratio > _threshold_good_point_ratio)
					{
						// then it is concave ellipses
						good_contour_type[good_contour_num]=0; // 0 stands for concave ellipse
						good_contour_index[good_contour_num]=i;
						good_contour_num++;
						//printf(">>>>A concave ellipse: goodPointRatio = %f\n", good2bad_ratio);
					}
				}////if (hull[0].size() >= 5)
			}
		}
	} //// for(i = 0; i < contourAll.size(); i++ )

//	// draw the good contour
//	for (i=0; i<good_contour_num; i++)
//	{
//	 drawContours(drawing, contourAll, good_contour_index[i], Scalar(0,255,0), 1, 8, hierarchy, 0, Point() );
//	}
//	imshow("contours", drawing);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// fit ellipse for each good contour
	vector<RotatedRect> minEllipse( 4*contourAll.size() ); // prepare enough room for ellipse
	vector<double> good_contour_error(4*contourAll.size()); //added by Kevin
	//minEllipse.reserve(4*contourAll.size()); // does NOT work
	int ellipse_num=0; // the number of all ellipse on the image
	for (i=0; i<good_contour_num; i++) // for each good contour
	{
		if (good_contour_type[i]==1) // normal ellipse
		{
			minEllipse[ellipse_num] = fitEllipse( Mat(contourAll[good_contour_index[i]]) ); // note the return is a RotatedRect
			if (max(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) / min(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) <_threshold_remove_thin_ellipse_ratio)
			{
				good_contour_error[i]= fcn_GetAlgError(contourAll[good_contour_index[i]], minEllipse[ellipse_num],contourAll[good_contour_index[i]].size());
				ellipse_num++;
			}
		}
		else if (good_contour_type[i]==0) // concave ellipse
		{
			//vector<vector<Point> > hull(1, int(contourAll[good_contour_index[i]].size()));
			vector<vector<Point> > hull(1, vector<Point> (int(contourAll[good_contour_index[i]].size()), Point()));
			convexHull( Mat(contourAll[good_contour_index[i]]), hull[0] );
			minEllipse[ellipse_num] = fitEllipse( Mat(hull[0]) );
			if (max(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) / min(minEllipse[ellipse_num].size.width ,minEllipse[ellipse_num].size.height) <_threshold_remove_thin_ellipse_ratio)
			{
				good_contour_error[i]= fcn_GetAlgError(contourAll[good_contour_index[i]], minEllipse[ellipse_num],contourAll[good_contour_index[i]].size());
				ellipse_num++;
			}
		}
//		else if (good_contour_type[i]==-1) // overlapped ellipse
//		{
//			vector<RotatedRect> overlapEllipses; //
//			int overlap_ellipse_num = fcn_partitionOverlappedEllipse(contourAll[good_contour_index[i]], overlapEllipses);
//			for (j=0; j<overlap_ellipse_num; j++)
//			{
//				minEllipse[ellipse_num]=overlapEllipses[j];
//				ellipse_num++;
//			}
//			//printf(">>>>An overlapped  ellipse: contain %d ellipses\n", overlap_ellipse_num);
//		}
	} //// for (i=0; i<good_contour_num; i++)
	//

	// Kevin Added: sort ellipse that have lowest algError
//	if(good_contour_num>0)
//	{
//	for (int i=0 ; i < good_contour_num ; i++)
//	{
//		good_contour_ellipse[i]= fitEllipse( Mat(contourAll[good_contour_index[i]]) );
//		good_contour_error[i]= fcn_GetAlgError(contourAll[good_contour_index[i]], good_contour_ellipse[i],contourAll[good_contour_index[i]].size());
//
//	}
//	}

//	cout <<"Total Good contours: " << ellipse_num << endl;
//	for (int i = 0 ; i < good_contour_index.size(); i++)
//	{
//		cout << "index: " << good_contour_index[i] << endl;
//		cout << "contour error: " << good_contour_error[i] << endl;
//		cout << "number of pts: " << contourAll[good_contour_index[i]].size() << endl;
//	}

int index_lowest_error = distance(good_contour_error.begin(), min_element (good_contour_error.begin(),good_contour_error.end()));
//if (index_lowest_error>0)
//{
//_index_best_fit = index_lowest_error - 1;
//}
//else
//	_index_best_fit = 0;
//	cout << "index of min error element: " << index_lowest_error-1 <<endl;
	_index_best_fit = index_lowest_error-1;
//	cout << "contour index: " << good_contour_index[index_lowest_error-1] << endl;
//
//	ellipse_best_fit = good_contour_ellipse[_index_best_fit];

	minEllipse_output=minEllipse; // return the pointer
	return ellipse_num; // return the ellipse number
}

double pond_detector::fcn_GetEllipseGoodPointRatio(vector<Point> contour, RotatedRect fittedEllipse, int pointNum, double threshold)
{
	// function description: determine how many points fits the given ellipse well
	double goodPointNum=0; // should be double, otherwise the ratio goodPointNum/pointNum is zero
	double a=max(fittedEllipse.size.width,fittedEllipse.size.height) / 2.0;
	double b=min(fittedEllipse.size.width,fittedEllipse.size.height) / 2.0;
	double alpha=fittedEllipse.angle/180*pi+pi/2;
	double xc=fittedEllipse.center.x;
	double yc=fittedEllipse.center.y;
	double algErr=0;
	for (int j=0; j<pointNum; j++)
	{
		algErr = fabs(
			pow(  ((contour[j].x-xc)*cos(alpha) + (contour[j].y-yc)*sin(alpha)) / a ,2  )
			+ pow(  ((contour[j].x-xc)*sin(alpha) - (contour[j].y-yc)*cos(alpha)) / b ,2  )
			-1 );
//		cout <<"AlgErr= " << algErr << endl;
		if (algErr<threshold)
		{
			goodPointNum++;
		}
	}
	return goodPointNum/pointNum;
}

double pond_detector::fcn_GetAlgError(vector<Point> contour, RotatedRect fittedEllipse, int pointNum)
{
	// function description: compute the algebraic error between some points and the fitted ellipse
	// ellipse parameter
	double a=max(fittedEllipse.size.width,fittedEllipse.size.height) / 2.0;
	double b=min(fittedEllipse.size.width,fittedEllipse.size.height) / 2.0;
	double alpha=fittedEllipse.angle/180*pi+pi/2;
	double xc=fittedEllipse.center.x;
	double yc=fittedEllipse.center.y;
	double algErr=0;
	for (int j=0; j<pointNum; j++)
	{
		algErr = algErr + fabs(
			pow(  ((contour[j].x-xc)*cos(alpha) + (contour[j].y-yc)*sin(alpha)) / a ,2  )
			+ pow(  ((contour[j].x-xc)*sin(alpha) - (contour[j].y-yc)*cos(alpha)) / b ,2  )
			-1 );
	}
	algErr=algErr/pointNum;

	return algErr;
}

void pond_detector::fcn_PlotEllipse(Mat& img, vector<RotatedRect> minEllipse, int ellipse_num, Scalar color)
{
	for (int i=0; i<ellipse_num; i++)
	{
		// ellipse
		ellipse( img, minEllipse[i], color, 2, 8 );
		// rotated rectangle
		Point2f rect_points[4];
		minEllipse[i].points( rect_points );
		for( int j = 0; j < 4; j++ )
			line( img, rect_points[j], rect_points[(j+1)%4], color, 2, 8 );
	}
}

void pond_detector::fcn_PlotEllipse(Mat& img, vector<RotatedRect> minEllipse, int ellipse_num, vector<int> cluster_idx, Scalar color)
{
	for(int i = 0; i< ellipse_num; i++ )
	{
		// ellipse
		ellipse( img, minEllipse[i], color, 2, 8 );
		// rotated rectangle
		Point2f rect_points[4];
		minEllipse[i].points( rect_points );
		for( int j = 0; j < 4; j++ )
			line( img, rect_points[j], rect_points[(j+1)%4], color, 1, 8 );
		// the center of the ellipse
		//fcn_DrawCross(img, minEllipse[i].center.x, minEllipse[i].center.y);
		//
		//char numStr[10];
		//sprintf(numStr, "%d(c%d)", i, cluster_idx[i]);
		//putText(img, numStr, Point(int(minEllipse[i].center.x), int(minEllipse[i].center.y)), FONT_HERSHEY_PLAIN, 1, color, 1, 8, false);
	}
}

void pond_detector::fcn_PlotEllipse(Mat& img, vector<RotatedRect> minEllipse, vector<int> idx, Scalar color, int flag)
{
	// if (flag==1)
	// the text put in the ellipse is the original index of the ellipse
	// otherwise, it is a consequential number
	for(int i = 0; i< idx.size(); i++ )
	{
		// ellipse
		ellipse( img, minEllipse[idx[i]], color, 2, 8 );
		//// rotated rectangle
		//Point2f rect_points[4];
		//minEllipse[idx[i]].points( rect_points );
		//for( int j = 0; j < 4; j++ )
		//	line( img, rect_points[j], rect_points[(j+1)%4], color, 2, 8 );
		// the center of the ellipse
		//fcn_DrawCross(img, minEllipse[idx[i]].center.x, minEllipse[idx[i]].center.y);
		//
		char numStr[10];
		if (abs(idx[i])<100 && abs(idx[i]<100)) // for safety, in case of numStr is too short
		{
			if (flag==1)
			{
				sprintf(numStr, "%d", idx[i]);
			}
			else
			{
				sprintf(numStr, "%d", i);
			}
			putText(img, numStr, Point(int(minEllipse[idx[i]].center.x), int(minEllipse[idx[i]].center.y)), FONT_HERSHEY_PLAIN, 1, Scalar(0,255,255), 2, 8, false);
		}
	}
}

void pond_detector::fcn_OneCirlcePoseEstimation_4Points(RotatedRect target, Mat& tvec)
	{
		// author: Zhang Mengmi
		//Input: the target ellipse and images for showing boundary points
		//return translation matrix t ( dim will be 3 by 1 )
		//directly solve using four non-colinear points in the circle
		Point center_Oc = target.center;
		double theta = target.angle/180*pi;
		double a = target.size.width/2;
		double b = target.size.height/2;
		double x = center_Oc.x;
		double y = center_Oc.y;
		// cout<<a<<" "<<b<<" "<<x<<" "<<y<<" "<<theta<<endl;

		//imaged circle coordinates
		Point2f a_u(x+a*cos(theta),y+a*sin(theta));
		Point2f a_l(x-a*cos(theta),y-a*sin(theta));
		Point2f b_u(x-b*sin(theta),y+b*cos(theta));
		Point2f b_l(x+b*sin(theta),y-b*cos(theta));
		//show four extracted points in image
		//circle(img, a_u,2,Scalar(0,0,0),-1,8,0);
		//circle(img, a_l,2,Scalar(0,0,0),-1,8,0);
		//circle(img, b_u,2,Scalar(0,0,0),-1,8,0);
		//circle(img, b_l,2,Scalar(0,0,0),-1,8,0);

		//circle coordinates in world frame
		//we set origin of circle as origin of world frame
		//circle radius to be 0.5 meter (50cm) in the world frame
		Point3f a_u_w(0,_diameter/2,0);
		Point3f a_l_w(0,-_diameter/2,0);
		Point3f b_u_w(_diameter/2,0,0);
		Point3f b_l_w(-_diameter/2,0,0);

		//correspondence is important
		vector<Point2f> imagePoints;
		vector<Point3f> objectPoints;

		imagePoints.push_back(a_u);
		imagePoints.push_back(a_l);
		imagePoints.push_back(b_u);
		imagePoints.push_back(b_l);

		objectPoints.push_back(a_u_w);
		objectPoints.push_back(a_l_w);
		objectPoints.push_back(b_u_w);
		objectPoints.push_back(b_l_w);

		Mat rvec=Mat::zeros(3,3,CV_64FC1);
		//Mat tvec=Mat::zeros(3,1,CV_64FC1);
		//Mat rotationMat;
		Mat zeroDistortionCoeffs=Mat::zeros(1, 5, CV_64FC1);
		solvePnP(objectPoints, imagePoints,_M1,zeroDistortionCoeffs,rvec,tvec); //image is already pre-undistorted
		//transform to the camera frame defined by ShiYu and show results
		//90 rotation needed for coordinates
		Mat R = (Mat_<double>(3,3) <<
			0, -1, 0,
			1, 0, 0,
			0, 0, 1);
		tvec = R * tvec;
		//tvec = tvec/100; //conversion to m from cm
		//cout<<"TranslationMat (coor in meter): "<<endl;
		//cout<<tvec<<endl;
	}
