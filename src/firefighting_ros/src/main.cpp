#include "opencv/cv.hpp"
#include <time.h>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <sstream>
#include <opencv2/features2d/features2d.hpp>

#include <ros/ros.h>

#include "visual_guide_ros.h"
//include STL containers
using namespace cv;
using namespace std;

#define ROS 0
#define DISPLAY 0
#define SHOW_TIME 1
#define SAVE_VIDEO 0

int main(int argc, char** argv)
{
    ros::init(argc, argv, "fire_visual_ros");
    
    fire_visual_ros s;
    
    ros::spin();

	return 1;
}		
