#include "visual_guide_ros.h"

fire_visual_ros::fire_visual_ros()
{
    uav_command = nh.subscribe("imav2visual", 10, &fire_visual_ros::uavMsgCallback, this);
	sub_image_raw = nh.subscribe("/camera/image_raw", 10, &fire_visual_ros::callback_sub_image_raw, this);

    flag = false;   

    fire_visual_pub_ = nh.advertise<serial_uav::MSG_UAV_ROS>("visual2imav", 1);
}

void fire_visual_ros::uavMsgCallback(const serial_uav::MSG_UAV_ROS::ConstPtr& msg)
{
	cout<<endl<<"cmd: "<<msg->cmd<<endl;
	if ((int)(msg->parameters[0]) == 1)
	{
		flag = true;

		uav_pose_msg.cmd = 215;
		uav_pose_msg.parameters.clear();

		uav_pose_msg.parameters.push_back(1);
		uav_pose_msg.parameters.push_back(2);
		uav_pose_msg.parameters.push_back(3);
		fire_visual_pub_.publish(uav_pose_msg);

		uav_pose_msg.parameters.clear();
	}
	else
	{
		flag = false;
	}	
}

void fire_visual_ros::callback_sub_image_raw (const sensor_msgs::Image::ConstPtr& msg_image_raw) 
{
    cv_bridge::CvImagePtr cv_ptr;
    try {
      cv_ptr = cv_bridge::toCvCopy(msg_image_raw, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e) {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    image_raw = cv_ptr->image;

	if (flag)
	{


		//calculateUAVPose();
	}
	else
	{
		//no image from camera
		ROS_INFO("%s\n", "Could not open or find the image");
	    uav_pose_msg.cmd = 215;
	}	    
}	

void fire_visual_ros::calculateUAVPose()
{

		pond_detector visual_detector;
		image_raw.copyTo(visual_detector.img_original);
		intrinsic.copyTo(visual_detector._M1);		
		visual_detector.detect();	
		
        pose_x = -visual_detector._t.at<double>(0,0);
        pose_y = -visual_detector._t.at<double>(1,0);
        pose_z = -visual_detector._t.at<double>(2,0); 	

		uav_pose_msg.parameters.clear();

		uav_pose_msg.parameters.push_back(pose_x);
		uav_pose_msg.parameters.push_back(pose_y);
		uav_pose_msg.parameters.push_back(pose_z);
		fire_visual_pub_.publish(uav_pose_msg);

		uav_pose_msg.parameters.clear();

}
