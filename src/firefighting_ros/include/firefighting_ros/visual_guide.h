#include <iostream>	// for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <vector>
#include <cmath>
#include <stdio.h>
#include <fstream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/opencv.hpp>

//include STL containers
using namespace cv;
using namespace std;

#define VDISPLAY 0

class pond_detector {

public:

	pond_detector()
	{
		s_threshold = "/home/shanmo/IMAV2015/vision_catkin/data/blue_thresh.txt";
		HSVParameterRead(s_threshold.c_str());

	   // Threshold parameters
		_threshold_algbraic_error=0.085; //default = 0.09 smaller means more strict
		_threshold_good_point_ratio= 0.4; //0.4; //0.45; // at least more than a half of the points on the contour should be on the ellipse, then the contour is a concave ellipse
		_threshold_center_collinear=4; //2; // pixel
		_threshold_remove_small_contour=20; // 20pixel;
		_threshold_remove_thin_ellipse_ratio=8;// default =2;
		_threshold_widthHeight_ratio_for_poseEst=2.5; //2.5	

		//fitting index
		_index_best_fit = 0;

		_diameter = 1;//pond diameter 				
	};

	void HSVParameterRead(const char* s);	
	void detect();

	void fcn_PlotEllipse(Mat& img, vector<RotatedRect> minEllipse, int ellipse_num, Scalar color);
	void fcn_PlotEllipse(Mat& img, vector<RotatedRect> minEllipse, int ellipse_num, vector<int> cluster_idx, Scalar color);
	void fcn_PlotEllipse(Mat& img, vector<RotatedRect> minEllipse, vector<int> idx, Scalar color, int flag);

	double fcn_GetEllipseGoodPointRatio(vector<Point> contour, RotatedRect fittedEllipse, int pointNum, double threshold);
	int fcn_partitionOverlappedEllipse(vector<Point> overlapContour, vector<RotatedRect>& overlapEllipses_output);
	double fcn_GetAlgError(vector<Point> contour, RotatedRect fittedEllipse, int pointNum);

	void fcn_OneCirlcePoseEstimation_4Points(RotatedRect target, Mat& tvec);

	int kev_DetectEllipseFromContour(vector<vector<Point> > contourAll, vector<Vec4i> hierarchy, vector<RotatedRect>& minEllipse_output, Mat& drawing);
	int bucket_DetectEllipseFromContour(vector<vector<Point> > contourAll, vector<Vec4i> hierarchy, vector<RotatedRect>& minEllipse_output, Mat& drawing);

	Mat equalizeIntensity(const Mat& inputImage);
	Mat illuminationCorrectionColorCLAHE (const Mat &img);

	Mat img_original;
	Mat img_undistorted;	
	Mat img_HSV;
	Mat img_binary;
	Mat img_publish;

	//text file location
	string s_threshold;

	//pond color
    Scalar _color_thresh_lower_water;
    Scalar _color_thresh_upper_water;	

    bool flag;

	RotatedRect _Target;
	Mat _t; //Initial Translation Matrix for first image	
	double _diameter;//diameter of pond
	Mat _M1; //Camera Intrinsic Matrix

private:

	static const double pi = 3.14159265358979323846;

   //Ellipse detection parameters
	double _threshold_algbraic_error;
	double _threshold_good_point_ratio;
	double _threshold_center_collinear;
	int _threshold_remove_small_contour;
	int _threshold_remove_thin_ellipse_ratio;
	int _threshold_widthHeight_ratio_for_poseEst;

	//Fitting error parameters
	int _index_best_fit, _index_best_fit_bucket;
}; 
