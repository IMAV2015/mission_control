#include "opencv/cv.hpp"
#include <time.h>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <sstream>
#include <opencv2/features2d/features2d.hpp>

//include STL containers
using namespace cv;
using namespace std;

/// PointGrey 14176655 Calibration Results -- 2014 Aug 04
double cch=242.24438 , ccw=324.44670, fch=340.69528  , fcw=341.54850;
Mat intrinsic = ( Mat_<double>(3, 3) << fcw, 0, ccw, 0, fch, cch, 0, 0, 1 );

double distortionCoeffs_array[5]= {-0.29314,   0.07105,   -0.00266,   0.00260,  0.00000};
double K_forDistort_array[9]= {fcw, 0, ccw, 0, fch, cch, 0, 0, 1};

Mat local_KforDistort(3,3,CV_64FC1, K_forDistort_array);
Mat distCoeffs(1, 5, CV_64FC1, distortionCoeffs_array); // distortion coefficients
