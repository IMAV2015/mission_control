#include <iostream>	// for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <vector>
#include <cmath>
#include <stdio.h>
#include <fstream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include "visual_guide.h"
#include "MSG_UAV_ROS.h"
//include STL containers
using namespace cv;
using namespace std;

class fire_visual_ros
{
public:
    fire_visual_ros();
    void uavMsgCallback(const serial_uav::MSG_UAV_ROS::ConstPtr& msg);
    void calculateUAVPose();
	void callback_sub_image_raw (const sensor_msgs::Image::ConstPtr& msg_image_raw); 

private:    
    ros::NodeHandle nh;	
	//receive uav command
    ros::Subscriber uav_command;
	//receive image
	ros::Subscriber sub_image_raw;
	size_t counter_sub_image_raw;
	std::vector<cv::Mat> raw_images;
	Mat image_raw;
	Mat intrinsic;
    bool flag;

    //publish pose
	ros::Publisher fire_visual_pub_;

    double pose_x;
    double pose_y;
    double pose_z;    

	serial_uav::MSG_UAV_ROS uav_pose_msg;     
};
