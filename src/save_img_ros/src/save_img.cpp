#include "save_img.h"

save_img_ros::save_img_ros()
{
    uav_command = nh.subscribe("imav2visual", 10, &save_img_ros::uavMsgCallback, this);
	
	sub_image_raw = nh.subscribe("/camera/image_color", 10, &save_img_ros::callback_sub_image_raw, this);
	time_t t = time(0);
	struct tm *ptm = localtime(&t);	
#if SAVE_DEFAULT_PATH
	sprintf(m_filenameimage, "/home/imav/images/stitch_default");
#else
	sprintf(m_filenameimage, "/home/imav/images/image%02d%02d%02d%02d%02d", ptm->tm_year-100, ptm->tm_mon+1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min);
#endif

	if (access(m_filenameimage, 0) != 0)
	{
		mkdir(m_filenameimage, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		ROS_INFO("%s\n","[Dlg] Error on creating the image folder");
	}
    counter = 1;

    flag = false;       
}

void save_img_ros::uavMsgCallback(const serial_uav::MSG_UAV_ROS::ConstPtr& msg)
{
	// cout<<endl<<"cmd: "<<msg->cmd<<endl;
	if ((msg->cmd == 210) && (msg->parameters[0] > 0.5))
	{
		flag = true;
		ROS_INFO("%s\n", "start image save...");
	}
	else
	{
		flag = false;
		ROS_INFO("%s\n", "stop image save...");		
	}	
}

void save_img_ros::callback_sub_image_raw (const sensor_msgs::Image::ConstPtr& msg_image_raw) 
{
    cv_bridge::CvImagePtr cv_ptr;
    try {
      cv_ptr = cv_bridge::toCvCopy(msg_image_raw, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e) {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    image_raw = cv_ptr->image;

	if (image_raw.data)
	{
	    sprintf(nameImage, "%s/img%04d.jpg", m_filenameimage, counter);
	    imwrite(nameImage, image_raw);
	    counter ++;  		
	}
	else
	{
		//no image from camera
		ROS_INFO("%s\n", "Could not open or find the image");
	}	    
}	

