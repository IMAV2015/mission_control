#include "opencv/cv.hpp"
#include <time.h>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <sstream>
#include <opencv2/features2d/features2d.hpp>

#include <ros/ros.h>

#include "save_img.h"
//include STL containers
using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "save_img_ros");
    ros::Time::init();
    ros::Rate rosRate(15);
    
    save_img_ros s;
    
    ros::spin();

	return 1;
}		
