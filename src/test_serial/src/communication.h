/*
 * communation.h
 *
 *  Created on: Jul 11, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_COMMUNICATION_H_
#define MISSION_CONTROL_SRC_COMMUNICATION_H_

#include <vector>
#include <iostream>
#include <cstdlib>
#include <iterator>

#include "serial_uav/MSG_UAV_ROS.h"
#include "ros/ros.h"


// command with UAV(Pixhawk)-----------
#define CMD_MISSION_BEGIN 1
#define CMD_MISSION_BEGIN_RECEIVED 2
#define CMD_MISSION_CONFIRM 3

#define CMD_TAKEOFF 10  // followed by height (1 double)
#define CMD_TAKEOFF_RECEIVED 11
#define CMD_TAKEOFF_OK 12

#define CMD_LAND 250   // no parameter needed.
#define CMD_LAND_RECEIVED 251

#define CMD_REF 30    // followed by ref_x, ref_y, ref_z, ref_c,   ref_vx, ref_vy, ref_vz, ref_vc,   ref_ax, ref_ay, ref_az, ref_ac (12 double)

#define CMD_REQUEST_MEA_BEGIN 21
#define CMD_MEA 22    // followed by mea_x, mea_y, mea_z, mea_c,   mea_vx, mea_vy, mea_vz, mea_vc,   mea_ax, mea_ay, mea_az, mea_ac (12 double)
#define CMD_REQUEST_MEA_STOP 28

#define CMD_REQUEST_GPS 51
#define CMD_RETURN_GPS 52 // followed by latitude, longitude, height, numberOfSatellite

#define CMD_REQUEST_HEADING 61
#define CMD_RETURN_HEADING 62 // followed by heading(in radian)

#define CMD_REQUEST_LOCATION 71
#define CMD_RETURN_LOCATION 72 // followed by x, y, z, c(in radian)
// command with UAV(Pixhawk)-----------

struct MSG_UAV{
	int cmd;
	std::vector<double> parameters;
};


class nodeCMM_serial{
private:
	static MSG_UAV receive;
	static bool serial_read_safe;

public:
	nodeCMM_serial(){

	}

	~nodeCMM_serial(){

	}

	void static send2UAV(ros::Publisher ros2serial, MSG_UAV msg);

	void static serial2ros_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg);

	MSG_UAV static getUAVMsg();
};




#endif /* MISSION_CONTROL_SRC_COMMUNICATION_H_ */
