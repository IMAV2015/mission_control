#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "serial_uav/MSG_UAV_ROS.h"

#include "communication.h"


using namespace std;

void subCallback(const serial_uav::MSG_UAV_ROS::ConstPtr& msg){
   cout<<endl<<"cmd: "<<msg->cmd<<endl;
}

int main(int argc, char **argv){

	ros::init(argc, argv, "test_serial");
	
	ros::NodeHandle nh;
	


	//ros::Subscriber sub = nh.subscribe("serial2ros", 1000, &nodeCMM_serial::serial2ros_callback);
	
	ros::Publisher pub = nh.advertise<serial_uav::MSG_UAV_ROS>("visual2imav", 1000);
	
	//ROS Spin
	ros::Rate rosRate(0.2);
	ros::AsyncSpinner spinner(2); // Use 2 threads
    spinner.start();
    
//    MSG_UAV msgToUAV;
//    MSG_UAV msgFromUAV;
//
//    //msgFromUAV = nodeCMM_serial::getUAVMsg();
//    //while(msgFromUAV)
//
//
//    while(nodeCMM_serial::getUAVMsg().cmd!=CMD_MISSION_BEGIN_RECEIVED){
//    	cout<<nodeCMM_serial::getUAVMsg().cmd<<endl;
//    	msgToUAV.cmd = CMD_MISSION_BEGIN;
//    	nodeCMM_serial::send2UAV(pub, msgToUAV);
//
//    	rosRate.sleep();
//    }
//
//    for(int i=0;i<5000;++i){
//		msgToUAV.cmd = CMD_MISSION_CONFIRM;
//		nodeCMM_serial::send2UAV(pub, msgToUAV);
//
//		rosRate.sleep();
//    }
//


//    msgToUAV.cmd = 1;
//    msgToUAV.parameters.push_back(100);
//    msgToUAV.parameters.push_back(200);
//    msgToUAV.parameters.push_back(300);
//    msgToUAV.parameters.push_back(410);
    
    serial_uav::MSG_UAV_ROS msg;
    msg.cmd = 215;
    msg.parameters.push_back(1);
    msg.parameters.push_back(1);
    msg.parameters.push_back(-10);

    while(ros::ok()){
    	pub.publish(msg);



    	rosRate.sleep();
    }
    


	return 1;
}
