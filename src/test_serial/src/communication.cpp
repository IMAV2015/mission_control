/*
 * communication.cpp
 *
 *  Created on: Jul 11, 2015
 *      Author: li
 */


#include "communication.h"


using namespace std;

MSG_UAV nodeCMM_serial::receive;
bool nodeCMM_serial::serial_read_safe = true;

void nodeCMM_serial::send2UAV(ros::Publisher ros2serial, MSG_UAV msg){

	serial_uav::MSG_UAV_ROS msgToUAV;
	msgToUAV.cmd = msg.cmd;
	msgToUAV.parameters = msg.parameters;

	ros2serial.publish(msgToUAV);


}

void nodeCMM_serial::serial2ros_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg){
	serial_read_safe = false;
	receive.cmd = ros_msg->cmd;
	receive.parameters = ros_msg->parameters;
	serial_read_safe = true;

}


MSG_UAV nodeCMM_serial::getUAVMsg(){
	if(serial_read_safe)
		return receive;
	else{
		MSG_UAV emptyMsg;
		emptyMsg.cmd = 0;
		return emptyMsg;
	}

}




