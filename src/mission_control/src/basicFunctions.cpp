/*
 * basicFunctions.cpp
 *
 *  Created on: Jul 12, 2015
 *      Author: li
 */

#include "basicFunctions.h"
#include "mission.h"

// ------------------------ parameters
double takeoff_height = 10;
double takeoff_wait = 5;

double cmd_auto_freq = 1;

double pp_takeoff_maxVel_xy  = 1;
double pp_takeoff_maxVel_z  = 1;
double pp_takeoff_maxVel_c  = 1;
double pp_takeoff_maxAcc_xy =  0.5;
double pp_takeoff_maxAcc_z  = 0.5;
double pp_takeoff_maxAcc_c =  0.5;

double pp_land_maxVel_xy  = 1;
double pp_land_maxVel_z  = 0.5;
double pp_land_maxVel_c  = 1;
double pp_land_maxAcc_xy =  0.5;
double pp_land_maxAcc_z  = 0.5;
double pp_land_maxAcc_c  = 0.5;

double pp_fly_maxVel_xy  = 3;
double pp_fly_maxVel_z  = 1;
double pp_fly_maxVel_c  = 1;
double pp_fly_maxAcc_xy  = 0.5;
double pp_fly_maxAcc_z  = 0.5;
double pp_fly_maxAcc_c  = 0.5;

// for height by water sensor
double safe_height = -5;
double hover_margin = 0.1;

// for QR code
int waypoint_src = 0;

double qr_height = -20;
double qr_height_2 = -5;
double qr_heading = 0;
double qr_land_lat = 0;
double qr_land_lon = 0;


// for visual guidance
double visual_guide_max_time = 60;

// ------------------------ parameters end


sem_t send2UAV_sem;
sem_t readUAV_sem;
sem_t send2Visual_sem;
sem_t readVisual_sem;

bool FLAG_MISSION_COMPLETE = false;
bool FLAG_MISSION_START = false;
bool FLAG_EXIT = false;


ros::Publisher ros2serial;
ros::Subscriber serial2ros;

ros::Publisher imav2visual;
ros::Subscriber visual2imav;

ros::Publisher imav2water;
ros::Subscriber water2imav;

#ifdef Cpp11
std::chrono::steady_clock::time_point uav::t_start = std::chrono::steady_clock::now();
#else
double uav::t_start = 0;
#endif
char cDir[FILENAME_MAX] = "arbitrary word is ok here. just for initialization of variable.";



double origin_mea_x = 0;
double origin_mea_y = 0;
double origin_mea_z = 0;
double origin_mea_c = 0;



void uav::px4Sound(){
	MSG_UAV msg;
	msg.cmd = CMD_PX4_SOUND;

	int counter = 0;

	while(ros::ok()){
		serialCMM.send2UAV(msg);
		counter+=1;

		usleep(200000);

		MSG_UAV msg_receive = serialCMM.getUAVMsg();
		if(msg_receive.cmd==CMD_PX4_SOUND_RECEIVED) {
			serialCMM.clearReceivedMsg();
			break;
		}
	}

}

double uav::getElapsedTime(){
	//double t_elapsed = (double)(clock() - uav::t_start) / CLOCKS_PER_SEC * 1000;
#ifdef Cpp11
	std::chrono::steady_clock::time_point t_end = std::chrono::steady_clock::now();
	//std::chrono::steady_clock::time_point diff = t_end - t_start;
	//double t_elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(diff).count();
	double t_elapsed = std::chrono::duration_cast<std::chrono::microseconds>(t_end-t_start).count() / 1000000.0;
#else
	double t_elapsed = ros::Time::now().toSec() - t_start;
#endif
	//double t_elapsed = 0;
	return t_elapsed;
}

void uav::printVector(std::vector<double>* src){
	for(int i=0;i<src->size();++i){
		std::cout.precision(12);
		std::cout<<src->at(i)<<", ";
	}
	std::cout<<"  (vecotr size: "<<src->size()<<")";
	std::cout<<std::endl;
}


int uav::readFromTxt(std::vector<uav::WayPnt>* wayPoints){
	int counter = 0;

	char path[100];
	std::strcat(path, cDir);
	std::strcat(path, "/WayPoint.txt");
	std::cout<<std::endl<<path<<std::endl;
	FILE *txtFILE;
	//txtFILE = std::fopen(path, "r");
	txtFILE = std::fopen("WayPoint.txt", "r");

	if(txtFILE == NULL){
		printf("Open file WayPoint.txt failed.\n");
		return 0;
	}


	while (1){

		WayPnt tempWayPoint;

		int readNum = fscanf(txtFILE, "%lf %lf %lf %lf %lf %d %d",
				&tempWayPoint.x, &tempWayPoint.y, &tempWayPoint.z, &tempWayPoint.c,
				&tempWayPoint.period, &tempWayPoint.target, &tempWayPoint.type);


		if(readNum==7)
			wayPoints->push_back(tempWayPoint);
		else
			break;

		counter += 1;

	}

	return wayPoints->size();
}

int uav::readFromQRCode(std::vector<uav::WayPnt>* wayPoints){
	char path[100];
	std::strcat(path, cDir);
	std::strcat(path, "/qr2gps.txt");
	std::cout<<std::endl<<path<<std::endl;
	FILE *txtFILE;
	txtFILE = std::fopen("qr2gps.txt", "r");

	if (txtFILE == NULL) {
		printf("Open file qr2gps.txt failed.\n");
		return 0;
	}

	double lattitude = 0;
	double longitude = 0;
	int readNum = fscanf(txtFILE, "%lf, %lf", &lattitude, &longitude);

	if(2==readNum){
		WayPnt tempWayPoint;

		// reach destination
		tempWayPoint.x = lattitude;
		tempWayPoint.y = longitude;
		tempWayPoint.z = qr_height;
		tempWayPoint.c = qr_heading;
		tempWayPoint.period = 0;
		tempWayPoint.target = TargetNone;
		tempWayPoint.type = MissionGPS;

		wayPoints->push_back(tempWayPoint);

		// reduce height.
		tempWayPoint.x = lattitude;
		tempWayPoint.y = longitude;
		tempWayPoint.z = qr_height_2 * 2;
		tempWayPoint.c = qr_heading;
		tempWayPoint.period = 0;
		tempWayPoint.target = TargetNone;
		tempWayPoint.type = MissionGPS;

		wayPoints->push_back(tempWayPoint);

		// reduce height, visual guidance, release payload
		tempWayPoint.x = lattitude;
		tempWayPoint.y = longitude;
		tempWayPoint.z = qr_height_2;
		tempWayPoint.c = qr_heading;
		tempWayPoint.period = 0;
		tempWayPoint.target = TargetRelease;
		if(visual_guide_max_time<=10) tempWayPoint.type = MissionGPS;
		else tempWayPoint.type = MissionVisualGuideXY;

		wayPoints->push_back(tempWayPoint);


		// recover height
		tempWayPoint.x = lattitude;
		tempWayPoint.y = longitude;
		tempWayPoint.z = qr_height;
		tempWayPoint.c = qr_heading;
		tempWayPoint.period = 0;
		tempWayPoint.target = TargetNone;
		tempWayPoint.type = MissionGPS;

		wayPoints->push_back(tempWayPoint);

		// reach landing position
		tempWayPoint.x = qr_land_lat;
		tempWayPoint.y = qr_land_lon;
		tempWayPoint.z = qr_height;
		tempWayPoint.c = qr_heading;
		tempWayPoint.period = 0;
		tempWayPoint.target = TargetNone;
		tempWayPoint.type = MissionGPS;

		wayPoints->push_back(tempWayPoint);

	}

	return wayPoints->size();
}

void uav::printWayPoints(std::vector<uav::WayPnt>* wayPoints){
	for(int i=0;i<wayPoints->size();++i){
		cout<<wayPoints->at(i).x<<"  "<<wayPoints->at(i).y<<"  "<<wayPoints->at(i).z<<"  "<<wayPoints->at(i).c<<"  "
				<<wayPoints->at(i).period<<"  "<<wayPoints->at(i).target<<"  "<<wayPoints->at(i).type<<"  "<<endl;
	}
}

void uav::readPara(){
	std::ifstream infile("parameters.txt");

	std::string line;
	while(std::getline(infile, line)){
		std::istringstream iss(line);
		std::string name;
		double value;
		if(!(iss>>name>>value)){
			continue;
		}

		if(name.c_str()[0] == '#'){
			//std::cout<<"detected"<<std::endl;
			continue;
		}
		else if(name.compare("takeoff_height")==0){
			takeoff_height = value;
		}
		else if(name.compare("takeoff_wait")==0){
			takeoff_wait = value;
		}
		else if(name.compare("cmd_auto_freq")==0){
			cmd_auto_freq = value;
		}
		else if(name.compare("pp_takeoff_maxVel_xy")==0){
			pp_takeoff_maxVel_xy = value;
		}
		else if(name.compare("pp_takeoff_maxVel_z")==0){
			pp_takeoff_maxVel_z = value;
		}
		else if(name.compare("pp_takeoff_maxVel_c")==0){
			pp_takeoff_maxVel_c = value;
		}
		else if(name.compare("pp_takeoff_maxAcc_xy")==0){
			pp_takeoff_maxAcc_xy = value;
		}
		else if(name.compare("pp_takeoff_maxAcc_z")==0){
			pp_takeoff_maxAcc_z = value;
		}
		else if(name.compare("pp_takeoff_maxAcc_c")==0){
			pp_takeoff_maxAcc_c = value;
		}
		else if(name.compare("pp_land_maxVel_xy")==0){
			pp_land_maxVel_xy = value;
		}
		else if(name.compare("pp_land_maxVel_z")==0){
			pp_land_maxVel_z = value;
		}
		else if(name.compare("pp_land_maxVel_c")==0){
			pp_land_maxVel_c = value;
		}
		else if(name.compare("pp_land_maxAcc_xy")==0){
			pp_land_maxAcc_xy = value;
		}
		else if(name.compare("pp_land_maxAcc_z")==0){
			pp_land_maxAcc_z = value;
		}
		else if(name.compare("pp_land_maxAcc_c")==0){
			pp_land_maxAcc_c = value;
		}
		else if(name.compare("pp_fly_maxVel_xy")==0){
			pp_fly_maxVel_xy = value;
		}
		else if(name.compare("pp_fly_maxVel_z")==0){
			pp_fly_maxVel_z = value;
		}
		else if(name.compare("pp_fly_maxVel_c")==0){
			pp_fly_maxVel_c = value;
		}
		else if(name.compare("pp_fly_maxAcc_xy")==0){
			pp_fly_maxAcc_xy = value;
		}
		else if(name.compare("pp_fly_maxAcc_z")==0){
			pp_fly_maxAcc_z = value;
		}
		else if(name.compare("pp_fly_maxAcc_c")==0){
			pp_fly_maxAcc_c = value;
		}
		else if(name.compare("waypoint_src")==0){
			waypoint_src = (int)value;
		}
		else if(name.compare("qr_height")==0){
			qr_height = value;
		}
		else if(name.compare("qr_height_2")==0){
			qr_height_2 = value;
		}
		else if(name.compare("qr_heading")==0){
			qr_heading = value;
		}
		else if(name.compare("qr_land_lat")==0){
			qr_land_lat = value;
		}
		else if(name.compare("qr_land_lon")==0){
			qr_land_lon = value;
		}
		else if (name.compare("safe_height") == 0) {
			safe_height = value;
		}
		else if (name.compare("hover_margin") == 0){
			hover_margin = value;
		}
		else if (name.compare("visual_guide_max_time") == 0){
			visual_guide_max_time = value;
		}
		else{
			std::cout<<name<<"  unknown!"<<std::endl;
		}



	}

	//std::cout<<take_off_height<<std::endl<<take_off_wait<<std::endl;
}








void initOtherByRelativeMea(double mea_x, double mea_y, double mea_z, double mea_c){
	origin_mea_x = mea_x;
	origin_mea_y = mea_y;
	origin_mea_z = mea_z;
	origin_mea_c = mea_c;

	safe_height += origin_mea_z;
}









