/*
 * missionLogic.h
 *
 *  Created on: Jul 19, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_MISSIONLOGIC_H_
#define MISSION_CONTROL_SRC_MISSIONLOGIC_H_

#define OUTDOOR_DEBUG_FLAG

#include "basicFunctions.h"
#include "communication.h"
#include "mission.h"
#include "pathPlanning.h"
#include "uavState.h"

void* logic(void* args);

void print_outdoor_debug();

#endif /* MISSION_CONTROL_SRC_MISSIONLOGIC_H_ */
