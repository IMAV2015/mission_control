/*
 * mission.h
 *
 *  Created on: Jul 12, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_MISSION_H_
#define MISSION_CONTROL_SRC_MISSION_H_

#include "basicFunctions.h"
#include "communication.h"
#include "uavState.h"
#include <iomanip>
#include <math.h>

#include "pathPlanning.h"




// mission description-------------
#define MissionGPS 1
#define MissionVisualGuideXY 2
#define MissionIndoor 3
#define MissionWaterZ 4
#define MissionLocation 10
#define MissionTakeOff 250
#define MissionLand 255

#define TargetNone 0
#define TargetGetWater 1
#define TargetDropWater 2
#define TargetRelease 23
#define TargetSaveImgBegin 14
#define TargetSaveImgEnd 15
// mission description-------------

//byc
//#define REFLEXXES_REACH_FLAG

struct MissionDesc{
	REFERENCE ref;
	int target;
	int type;
};


class CurrentMission {
public:
	MEASUREMENT mea;
	REFERENCE ref;
	int type;
	int target;

	// target process
	bool is_targetBegin;
	bool is_targetFinish;

	double targetBeginTime;
	double targetFinishTime;

private:
	bool flag_visited;
	bool flag_period;
	bool flag_addition;

	double beginTime;

	double c_old;
	double delta;

	pthread_mutex_t mission_ref_mutex;
	pthread_mutex_t mission_mea_mutex;



public:
	CurrentMission(REFERENCE r, int target_in, int type_in){
		ref.x = r.x;
		ref.y = r.y;
		ref.z = r.z;
		ref.c = r.c;   // c in radian.
		ref.period = r.period;

		type = type_in;
		target = target_in;
		if(target_in==TargetNone)
			flag_addition = 1;
		else
			flag_addition = 0;
		flag_visited = 0;
		flag_period = 0;

		c_old = r.c;
		delta = 0;



		pthread_mutex_init (&mission_ref_mutex,NULL);
		pthread_mutex_init (&mission_mea_mutex,NULL);
	}

	~CurrentMission(){
		pthread_mutex_destroy(&mission_ref_mutex);
		pthread_mutex_destroy(&mission_mea_mutex);
	}

	void setBeginTime(){
		beginTime = uav::getElapsedTime();
		// re init
		if(target==TargetNone)  flag_addition = 1;
		else flag_addition = 0;

		is_targetBegin = false;
		is_targetFinish = false;
		targetBeginTime = 0;
		targetFinishTime = 0;
	}
	bool checkPeriod(){
		double currTime = uav::getElapsedTime();
		//printf("CurrTime: %f, BeginTime: %f\n", currTime, beginTime);
		if(currTime - beginTime >= ref.period)
			return true;
		else
			return false;
	}
	bool checkVisit();
	void setAdditionFlag(bool flag){
		flag_addition = flag;
	}
	bool getAdditionFlag(){
		return flag_addition;
	}
	bool checkFlag(){
		flag_visited = checkVisit();
		flag_period = checkPeriod();
		if(flag_visited && flag_period && flag_addition) return true;
		else return false;
	}
	void printFlag(){
		flag_visited = checkVisit();
		flag_period = checkPeriod();

		if (flag_visited)
			printf("Reference arrived.\n");
		else
			printf("Reference NOT arrived.\n");

		if (flag_period)
			printf("Period satisfied.\n");
		else{
			printf("Period NOT satisfied. begin %f, ref %f\n", beginTime, ref.period);
		}

		if (flag_addition)
			printf("Addition condition satisfied.\n");
		else
			printf("Addition condition NOT satisfied.\n");
	}

	void printFalseFlag(){
		flag_visited = checkVisit();
		flag_period = checkPeriod();

		if (!flag_visited)
			printf("Reference NOT arrived.\n");

		if (!flag_period)
			printf("Period NOT satisfied.\n");

		if (!flag_addition)
			printf("Addition condition NOT satisfied.\n");
	}

	MEASUREMENT getMeasurement(){
		Lock lck(mission_mea_mutex);
		return mea;
	}

	REFERENCE getReference(){
		Lock lck(mission_ref_mutex);
		return ref;
	}

	void setMeasurement(double x, double y, double z, double c){
		Lock lck(mission_mea_mutex);
		mea.x = x;
		mea.y = y;
		mea.z = z;
		mea.c = INPI(c);
		//mea.c = uav::getElapsedTime();

	}
	void setReference(REFERENCE src, double oldC){
		Lock lck(mission_ref_mutex);

		c_old = INPI(oldC);

		ref.x = src.x;
		ref.y = src.y;
		ref.z = src.z;
		ref.c = INPI(src.c);
		ref.period = src.period;

		//printf("old_c, new_c: %f,  %f  -------------------\n", c_old, ref.c);
		if(ref.c - c_old > 1.01*M_PI){
			delta -= 2*M_PI;

		}
		else if(ref.c-c_old < -1.01*M_PI){
			delta += 2*M_PI;
			//printf("old_c, new_c: %f,  %f  -------------------\n", c_old, ref.c);
		}
		ref.c += delta;



		// re init
		if(target==TargetNone)  flag_addition = 1;
		else flag_addition = 0;

		is_targetBegin = false;
		is_targetFinish = false;
		targetBeginTime = 0;
		targetFinishTime = 0;

	}
	void setReferenceXY(double x, double y){
		Lock lck(mission_ref_mutex);
		ref.x = x;
		ref.y = y;
	}
	void setReferenceZ(double z){
		Lock lck(mission_ref_mutex);
		ref.z = z;
	}
	void setPeroid(double p){
		Lock lck(mission_ref_mutex);
		ref.period = p;
	}


	void printMeasurement(){
		printf("[MISSION] measurement: %6.2f, %6.2f, %6.2f, %6.2f, %6.2f\n", mea.x, mea.y, mea.z, mea.c, uav::getElapsedTime()-beginTime);
	}
	void printReference(){
		printf("[MISSION] reference: %6.2f, %6.2f, %6.2f, %6.2f, %6.2f\n", ref.x, ref.y, ref.z, ref.c, ref.period);
	}

	double getDelta(){
		return delta;
	}


	void setMissionFromSet(std::vector<MissionDesc> &missionSet, int missionIndex);
};


bool initializeMissionSet(const std::vector<uav::WayPnt> src, std::vector<MissionDesc>& dst, ros::Publisher ros2serial);

void printMissionSet(std::vector<MissionDesc>& src);


extern CurrentMission mission;



// target process
void processTraget();
void getWater();
void dropWater();
void stopWater();
void releasePayload();
void saveImgBegin();
void saveImgEnd();


// ----------------------- external mission
// for MissionVisualGuideXY
class UAV_VisualXY{
private:
	double beginTime, endTime;
	bool is_begin, is_end;
public:
	UAV_VisualXY(){
		is_begin = false;
		is_end = false;
		beginTime = 0;
		endTime = 0;
	}
	~UAV_VisualXY(){

	}

	bool isMissionVisualGuideXYDone();
	std::vector<double> getVisualGuidanceXY();
};
extern UAV_VisualXY visualXY;


// MissionWaterZ
enum WaterState{
	DESCENDING,
	HOVERING,
	ASCENDING
};
class UAV_Water{
private:
	double dynamicZ;
	enum WaterState uav_on_water;

	double inWaterTimeBegin;

public:
	UAV_Water(){
		uav_on_water = DESCENDING;
		dynamicZ = -10;
	}
	~UAV_Water(){}

	bool isMissionWaterZDone();
	double getZByWaterSensor();
};
extern UAV_Water waterZ;


#endif /* MISSION_CONTROL_SRC_MISSION_H_ */
