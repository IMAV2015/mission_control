/*
 * pathPlanning.cpp
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 */


#include "pathPlanning.h"


void GPSReflexxes::initializeGPSPathPlanning(UAV_STATE state){
	IP->CurrentPositionVector->VecData[0] = state.x_pos;
	IP->CurrentPositionVector->VecData[1] = state.y_pos;
	IP->CurrentPositionVector->VecData[2] = state.z_pos;
	IP->CurrentPositionVector->VecData[3] = state.c_pos;

	IP->CurrentVelocityVector->VecData[0] = state.x_vel;
	IP->CurrentVelocityVector->VecData[1] = state.y_vel;
	IP->CurrentVelocityVector->VecData[2] = state.z_vel;
	IP->CurrentVelocityVector->VecData[3] = state.c_vel;

	IP->CurrentAccelerationVector->VecData[0] = state.x_acc;
	IP->CurrentAccelerationVector->VecData[1] = state.y_acc;
	IP->CurrentAccelerationVector->VecData[2] = state.z_acc;
	IP->CurrentAccelerationVector->VecData[3] = state.c_acc;

	is_initialized = true;

}

void GPSReflexxes::planningSimple(REFERENCE target){
	IP->TargetPositionVector->VecData[0] = target.x;
	IP->TargetPositionVector->VecData[1] = target.y;
	IP->TargetPositionVector->VecData[2] = target.z;
	IP->TargetPositionVector->VecData[3] = target.c;

	IP->TargetVelocityVector->VecData[0] = 0;
	IP->TargetVelocityVector->VecData[1] = 0;
	IP->TargetVelocityVector->VecData[2] = 0;
	IP->TargetVelocityVector->VecData[3] = 0;

	int status = RML->RMLPosition(*IP, OP, Flags);
	if(status<0)
		cout<<" !!! Error in simple path planning."<<endl;
	else{
		setGeneratedRef();
	}
	//byc
	reflexxes_status = status;

	// ensure that reference to UAV is in pi
	INPI(ref_pos.at(3));

	*IP->CurrentPositionVector = *OP->NewPositionVector;
	*IP->CurrentVelocityVector = *OP->NewVelocityVector;
	*IP->CurrentAccelerationVector = *OP->NewAccelerationVector;
}

void GPSReflexxes::planningWithState(REFERENCE target, UAV_STATE state){
	// set current conditions
	IP->CurrentPositionVector->VecData[0] = state.x_pos;
	IP->CurrentPositionVector->VecData[1] = state.y_pos;
	IP->CurrentPositionVector->VecData[2] = state.z_pos;
	IP->CurrentPositionVector->VecData[3] = state.c_pos;

	IP->CurrentVelocityVector->VecData[0] = state.x_vel;
	IP->CurrentVelocityVector->VecData[1] = state.y_vel;
	IP->CurrentVelocityVector->VecData[2] = state.z_vel;
	IP->CurrentVelocityVector->VecData[3] = state.c_vel;

	IP->CurrentAccelerationVector->VecData[0] = state.x_acc;
	IP->CurrentAccelerationVector->VecData[1] = state.y_acc;
	IP->CurrentAccelerationVector->VecData[2] = state.z_acc;
	IP->CurrentAccelerationVector->VecData[3] = state.c_acc;


	// set target conditions
	IP->TargetPositionVector->VecData[0] = target.x;
	IP->TargetPositionVector->VecData[1] = target.y;
	IP->TargetPositionVector->VecData[2] = target.z;
	IP->TargetPositionVector->VecData[3] = target.c;

	IP->TargetVelocityVector->VecData[0] = 0;
	IP->TargetVelocityVector->VecData[1] = 0;
	IP->TargetVelocityVector->VecData[2] = 0;
	IP->TargetVelocityVector->VecData[3] = 0;

	// get planning result.
	int status = RML->RMLPosition(*IP, OP, Flags);
	if (status < 0)
		cout << " !!! Error in path planning according to UAV state." << endl;
	else {
		setGeneratedRef();
	}
	// ensure that reference to UAV is in pi
	INPI(ref_pos.at(3));
}

void GPSReflexxes::setGeneratedRef(){
	Lock lck(pp_ref_mutex);

	for (int i = 0; i < NUMBER_DOF; ++i) {
		ref_pos.at(i) = OP->NewPositionVector->VecData[i];
		ref_vel.at(i) = OP->NewVelocityVector->VecData[i];
		ref_acc.at(i) = OP->NewAccelerationVector->VecData[i];
	}
	ref_vel.at(3) = 0;
	ref_acc.at(3) = 0;

}


void GPSReflexxes::sentReference2UAV(ros::Publisher ros2serial){
	MSG_UAV ref;
	ref.cmd = CMD_REF;
	for(int i=0;i<NUMBER_DOF;++i){
		ref.parameters.push_back(ref_pos.at(i));
	}
	for (int i = 0; i < NUMBER_DOF; ++i) {
		ref.parameters.push_back(ref_vel.at(i));
	}
	for (int i = 0; i < NUMBER_DOF; ++i) {
		ref.parameters.push_back(ref_acc.at(i));
	}

	serialCMM.send2UAV(ref);
}


void GPSReflexxes::sendManualPosRef2UAV(ros::Publisher ros2serial, vector<double> &ref_pos_in){
	Lock lck(pp_ref_mutex);
	for(int i=0;i<NUMBER_DOF;++i){
		ref_pos.at(i) = ref_pos_in.at(i);
	}
	lck.~Lock();

	MSG_UAV ref;
	ref.cmd = CMD_REF;
	for (int i = 0; i < NUMBER_DOF; ++i) {
		ref.parameters.push_back(ref_pos.at(i));
	}
	for (int i = 0; i < NUMBER_DOF; ++i) {
		ref.parameters.push_back(0);
	}
	for (int i = 0; i < NUMBER_DOF; ++i) {
		ref.parameters.push_back(0);
	}
	serialCMM.send2UAV(ref);
}



GPSReflexxes pp;
