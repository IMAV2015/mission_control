/*
 * uavState.h
 *
 *  Created on: Aug 25, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_UAVSTATE_H_
#define MISSION_CONTROL_SRC_UAVSTATE_H_

#include "basicFunctions.h"
#include "communication.h"
#include "mission.h"

class UAV_state{
private:
	UAV_STATE uav_state;
	pthread_mutex_t state_mutex;
	double updateTime;

public:
	UAV_state(){
		updateTime = 0;
		pthread_mutex_init(&state_mutex, NULL);
	}

	~UAV_state(){
		pthread_mutex_destroy(&state_mutex);
	}

	UAV_STATE getUAVState();
	void setUAVState(std::vector<double> src);
};

extern UAV_state state_uav;

void* uavState(void* args);



#endif /* MISSION_CONTROL_SRC_UAVSTATE_H_ */
