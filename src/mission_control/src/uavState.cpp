/*
 * uavState.cpp
 *
 *  Created on: Aug 25, 2015
 *      Author: li
 */

#include "uavState.h"

UAV_state state_uav;

UAV_STATE UAV_state::getUAVState(){
	Lock lck(state_mutex);
	return uav_state;
}
void UAV_state::setUAVState(std::vector<double> src){
	Lock lck(state_mutex);

	uav_state.x_pos = src.at(0);
	uav_state.y_pos = src.at(1);
	uav_state.z_pos = src.at(2);
	uav_state.c_pos = src.at(3);

	uav_state.x_vel = src.at(4);
	uav_state.y_vel = src.at(5);
	uav_state.z_vel = src.at(6);
	uav_state.c_vel = src.at(7);

	uav_state.x_acc = src.at(8);
	uav_state.y_acc = src.at(9);
	uav_state.z_acc = src.at(10);
	uav_state.c_acc = src.at(11);

	uav_state.a_pos = src.at(12);
	uav_state.b_pos = src.at(13);
	uav_state.cc_pos = src.at(14);

	updateTime = uav::getElapsedTime();

}




void* uavState(void* args){
	cout<<"[INIT] UAV state ready."<<endl;

	ros::Rate rosRate(FREQUENCY);

	MSG_UAV msg_receive;

	while(ros::ok()){
		// set measurement
		msg_receive = serialCMM.getUAVMsg();
		if (msg_receive.cmd == CMD_RETURN_MEA && msg_receive.parameters.size()==15) {
			mission.setMeasurement(msg_receive.parameters.at(0),
					msg_receive.parameters.at(1),
					msg_receive.parameters.at(2),
					msg_receive.parameters.at(3));

			state_uav.setUAVState(msg_receive.parameters);

		}

		rosRate.sleep();

	}


}


