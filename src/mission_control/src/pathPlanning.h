/*
 * pathPlanning.h
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_PATHPLANNING_H_
#define MISSION_CONTROL_SRC_PATHPLANNING_H_

#include "basicFunctions.h"
#include "communication.h"
#include "uavState.h"

#include "ReflexxesAPI.h"
#include "RMLPositionFlags.h"
#include "RMLPositionInputParameters.h"
#include "RMLPositionOutputParameters.h"

using namespace std;


#define NUMBER_DOF 4

#define PP_TAKEOFF 1
#define PP_LAND 255
#define PP_GPS 10
#define PP_LAND_SLOW 11


class GPSReflexxes{
public:
	vector<double> ref_pos;
	vector<double> ref_vel;
	vector<double> ref_acc;

	bool is_initialized;

	//byc
	int reflexxes_status;
private:
	ReflexxesAPI *RML; /*!< Reflexxes RML API variable */
	RMLPositionInputParameters *IP; /*!< Reflexxes input variable */
	RMLPositionOutputParameters *OP; /*!< Reflexxes output variable */
	RMLPositionFlags Flags; /*!< Reflexxes position flags */

	ReflexxesAPI *velRML; /*!< Reflexxes velocity API */
	RMLVelocityInputParameters *velIP; /*!< Reflexxes velocity mode input */
	RMLVelocityOutputParameters *velOP; /*!< Reflexxes velocity mode output */
	RMLVelocityFlags velFlags; /*!< Reflexxes velocity mode flags */

	double maxvel[4];// = {3,3,1.5,1};
	double maxacc[4];// = {2,2,1,1};
	double maxjerk[4];// = {1,1,1,1};

	pthread_mutex_t pp_ref_mutex;

public:
	GPSReflexxes(){
//		ref_pos = {0,0,0,0};
//		ref_vel = {0,0,0,0};
//		ref_acc = {0,0,0,0};

		for(int i=0;i<4;++i){
			ref_pos.push_back(0);
			ref_vel.push_back(0);
			ref_acc.push_back(0);
		}
		maxvel[0]=3;maxvel[1]=3;maxvel[2]=0.5;maxvel[3]=1;
		maxacc[0]=0.5;maxacc[1]=0.5;maxacc[2]=0.5;maxacc[3]=0.5;
		maxjerk[0]=5;maxjerk[1]=5;maxjerk[2]=1;maxjerk[3]=1;

		RML = new ReflexxesAPI(NUMBER_DOF, CYCLE_TIME);
		IP = new RMLPositionInputParameters(NUMBER_DOF);
		OP = new RMLPositionOutputParameters(NUMBER_DOF);

		IP->MaxVelocityVector->VecData [0] = maxvel[0]; //3.0		;
		IP->MaxVelocityVector->VecData [1] = maxvel[1];//3.0		;
		IP->MaxVelocityVector->VecData [2] = maxvel[2];//1.5		;
		IP->MaxVelocityVector->VecData [3] = maxvel[3];

		IP->MaxAccelerationVector->VecData [0] = maxacc[0];
		IP->MaxAccelerationVector->VecData [1] = maxacc[1];
		IP->MaxAccelerationVector->VecData [2] = maxacc[2];
		IP->MaxAccelerationVector->VecData [3] = maxacc[3];

		IP->MaxJerkVector->VecData [0] = maxjerk[0];
		IP->MaxJerkVector->VecData [1] = maxjerk[1];
		IP->MaxJerkVector->VecData [2] = maxjerk[2];
		IP->MaxJerkVector->VecData [3] = maxjerk[3];

		IP->SelectionVector->VecData[0] = true;
		IP->SelectionVector->VecData[1] = true;
		IP->SelectionVector->VecData[2] = true;
		IP->SelectionVector->VecData[3] = true;

		is_initialized = false;
		
		reflexxes_status = 0;//byc

		pthread_mutex_init (&pp_ref_mutex,NULL);

	}

	~GPSReflexxes(){
		RML->~ReflexxesAPI();
		IP->~RMLPositionInputParameters();
		OP->~RMLPositionOutputParameters();

		velRML->~ReflexxesAPI();
		velIP->~RMLVelocityInputParameters();
		velIP->~RMLVelocityInputParameters();

		pthread_mutex_destroy(&pp_ref_mutex);
	}

	void initializeGPSPathPlanning(UAV_STATE state);

	void planningSimple(REFERENCE target);

	void planningWithState(REFERENCE target, UAV_STATE state);

	void sentReference2UAV(ros::Publisher ros2serial);

	void sendManualPosRef2UAV(ros::Publisher ros2serial, vector<double> &ref_pos_in);

	void setGeneratedRef();

	vector<double> getRefPos(){
		Lock lck(pp_ref_mutex);
		return ref_pos;
	}
	vector<double> getRefVel(){
		Lock lck(pp_ref_mutex);
		return ref_vel;
	}
	vector<double> getRefAcc(){
		Lock lck(pp_ref_mutex);
		return ref_acc;
	}

	void resetPathPlanning(int type){
		is_initialized = false;
		reflexxes_status = 0;//byc
		switch (type){
			case PP_TAKEOFF:{
				maxvel[0]=pp_takeoff_maxVel_xy;
				maxvel[1]=pp_takeoff_maxVel_xy;
				maxvel[2]=pp_takeoff_maxVel_z;
				maxvel[3]=pp_takeoff_maxVel_c;

				maxacc[0]=pp_takeoff_maxAcc_xy;
				maxacc[1]=pp_takeoff_maxAcc_xy;
				maxacc[2]=pp_takeoff_maxAcc_z;
				maxacc[3]=pp_takeoff_maxAcc_c;
				break;
			}
			case PP_LAND:{
				maxvel[0]=pp_land_maxVel_xy;
				maxvel[1]=pp_land_maxVel_xy;
				maxvel[2]=pp_land_maxVel_z;
				maxvel[3]=pp_land_maxVel_c;

				maxacc[0]=pp_land_maxAcc_xy;
				maxacc[1]=pp_land_maxAcc_xy;
				maxacc[2]=pp_land_maxAcc_z;
				maxacc[3]=pp_land_maxAcc_c;
				break;
			}
			case PP_GPS:{
				maxvel[0]=pp_fly_maxVel_xy;
				maxvel[1]=pp_fly_maxVel_xy;
				maxvel[2]=pp_fly_maxVel_z;
				maxvel[3]=pp_fly_maxVel_c;

				maxacc[0]=pp_fly_maxAcc_xy;
				maxacc[1]=pp_fly_maxAcc_xy;
				maxacc[2]=pp_fly_maxAcc_z;
				maxacc[3]=pp_fly_maxAcc_c;
				break;
			}
			case PP_LAND_SLOW:{
				maxvel[0]=pp_land_maxVel_xy;
				maxvel[1]=pp_land_maxVel_xy;
				maxvel[2]=0.2;
				maxvel[3]=pp_land_maxVel_c;

				maxacc[0]=pp_land_maxAcc_xy;
				maxacc[1]=pp_land_maxAcc_xy;
				maxacc[2]=pp_land_maxAcc_z;
				maxacc[3]=pp_land_maxAcc_c;
				break;
			}
			default:{
				break;
			}
		}

		IP->MaxVelocityVector->VecData [0] = maxvel[0]; //3.0		;
		IP->MaxVelocityVector->VecData [1] = maxvel[1];//3.0		;
		IP->MaxVelocityVector->VecData [2] = maxvel[2];//1.5		;
		IP->MaxVelocityVector->VecData [3] = maxvel[3];

		IP->MaxAccelerationVector->VecData [0] = maxacc[0];
		IP->MaxAccelerationVector->VecData [1] = maxacc[1];
		IP->MaxAccelerationVector->VecData [2] = maxacc[2];
		IP->MaxAccelerationVector->VecData [3] = maxacc[3];

		IP->MaxJerkVector->VecData [0] = maxjerk[0];
		IP->MaxJerkVector->VecData [1] = maxjerk[1];
		IP->MaxJerkVector->VecData [2] = maxjerk[2];
		IP->MaxJerkVector->VecData [3] = maxjerk[3];

		IP->SelectionVector->VecData[0] = true;
		IP->SelectionVector->VecData[1] = true;
		IP->SelectionVector->VecData[2] = true;
		IP->SelectionVector->VecData[3] = true;

	}
};

extern GPSReflexxes pp;


#endif /* MISSION_CONTROL_SRC_PATHPLANNING_H_ */
