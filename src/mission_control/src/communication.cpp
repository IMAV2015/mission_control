/*
 * communication.cpp
 *
 *  Created on: Jul 11, 2015
 *      Author: li
 */


#include "communication.h"



using namespace std;


nodeCMM_serial serialCMM;

void nodeCMM_serial::send2UAV(MSG_UAV msg){

	serial_uav::MSG_UAV_ROS msgToUAV;
	msgToUAV.cmd = msg.cmd;
	msgToUAV.parameters = msg.parameters;

	ros2serial.publish(msgToUAV);

	sent = msg;
	sem_post(&send2UAV_sem);


}
void nodeCMM_serial::serial2ros_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg){
	Lock lck(readUAVMsg_mutex);


	receive.cmd = ros_msg->cmd;
	receive.parameters = ros_msg->parameters;

	receive_auto_clear.cmd = ros_msg->cmd;
	receive_auto_clear.parameters = ros_msg->parameters;


	if(ros_msg->cmd==CMD_RETURN_GPS){
		receive.parameters.at(0) = receive.parameters.at(0) / (double)10000;
		receive.parameters.at(1) = receive.parameters.at(1) / (double)10000;

		receive_auto_clear.parameters.at(0) = receive_auto_clear.parameters.at(0) / (double)10000;
		receive_auto_clear.parameters.at(1) = receive_auto_clear.parameters.at(1) / (double)10000;
	}


	for (int i=0;i<Receive_Buffer_Length-1;++i){
		receive_history.at(i) = receive_history.at(i+1);
	}
	receive_history.at(Receive_Buffer_Length-1) = receive;



	sem_post(&readUAV_sem);
}
MSG_UAV nodeCMM_serial::getUAVMsg(){
	Lock lck(readUAVMsg_mutex);

	return receive;

}
MSG_UAV nodeCMM_serial::getUAVMsgOnce(){
	Lock lck(readUAVMsg_mutex);

	MSG_UAV temp;


	temp.cmd = receive_auto_clear.cmd;
	temp.parameters = receive_auto_clear.parameters;

	receive_auto_clear.cmd = 0;
	receive_auto_clear.parameters.clear();

	return temp;
}
void nodeCMM_serial::clearReceivedMsg(){
	Lock lck(readUAVMsg_mutex);

	receive.cmd = 0;
	receive.parameters.clear();
}
vector<MSG_UAV> nodeCMM_serial::getUAVMsgBuffer(){
	Lock lck(readUAVMsg_mutex);

	return receive_history;
}
MSG_UAV nodeCMM_serial::getSentUAVMsg(){
	return sent;
}



nodeCMM_visual visualCMM;

void nodeCMM_visual::send2Visual(MSG_UAV msg){
	serial_uav::MSG_UAV_ROS msg2Visual;
	msg2Visual.cmd = msg.cmd;
	msg2Visual.parameters = msg.parameters;

	imav2visual.publish(msg2Visual);
	sent = msg;

	sem_post(&send2Visual_sem);
}
void nodeCMM_visual::visual2imav_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg){
	Lock lck(readVisualMsg_mutex);

	receive.cmd = ros_msg->cmd;
	receive.parameters = ros_msg->parameters;

	sem_post(&readVisual_sem);
}
MSG_UAV nodeCMM_visual::getMsg(){
	Lock lck(readVisualMsg_mutex);

	return receive;
}
void nodeCMM_visual::clearReceivedMsg(){
	Lock lck(readVisualMsg_mutex);

	receive.cmd = 0;
	receive.parameters.clear();
}
MSG_UAV nodeCMM_visual::getMsgOnce(){
	Lock lck(readVisualMsg_mutex);

	MSG_UAV temp;
	temp.cmd = receive.cmd;
	temp.parameters = receive.parameters;

	receive.cmd = 0;
	receive.parameters.clear();

	return temp;
}
MSG_UAV nodeCMM_visual::getSentMsg(){
	return sent;
}
void nodeCMM_visual::printMsg(MSG_UAV msg){
	cout<<msg.cmd<<"  -  ";
	for(int i=0;i<msg.parameters.size();++i){
		cout<<msg.parameters.at(i)<<", ";
	}
	cout<<endl;
}




nodeCMM_water waterCMM;
void nodeCMM_water::send2Water(std_msgs::UInt16 msg){
	imav2water.publish(msg);
	sent = msg;
}
void nodeCMM_water::water2imav_callback(const std_msgs::UInt16::ConstPtr& ros_msg){
	receive = *ros_msg;
}
std_msgs::UInt16 nodeCMM_water::getMsg(){
	return receive;
}

//----------------- for user command
int autoCMD = AUTO_CMD_NONE;

void* userCommandReturn(void* args){
	ros::Rate rosRate(FREQUENCY);

	while(!FLAG_MISSION_START && !FLAG_EXIT){
		MSG_UAV msg = serialCMM.getUAVMsgOnce();
		if (msg.cmd == CMD_RETURN_GPS
				|| msg.cmd == CMD_RETURN_LOCATION
				|| msg.cmd == CMD_RETURN_HEADING
				|| msg.cmd == CMD_TAKEOFF_RECEIVED
				|| msg.cmd == CMD_TAKEOFF_OK
				|| msg.cmd == CMD_LAND_RECEIVED) {
			cout << "Received from UAV:" << endl;
			cout << msg.cmd << "  ";
			uav::printVector(&msg.parameters);
			cout<<endl;
		}

		rosRate.sleep();
	}

	cout<<"[EXIT] User command disabled."<<endl;
}

void* autoCommand(void* args){
	ros::Rate rosRate((int)cmd_auto_freq);

	while(ros::ok()){
		if(autoCMD==AUTO_CMD_GPS){
			MSG_UAV msg;
			msg.cmd = CMD_REQUEST_GPS;
			serialCMM.send2UAV(msg);
		}
		else if(autoCMD==AUTO_CMD_LOCATION){
			MSG_UAV msg;
			msg.cmd = CMD_REQUEST_LOCATION;
			serialCMM.send2UAV(msg);
		}
		else if(autoCMD==AUTO_CMD_NONE){

		}
		else if(autoCMD==AUTO_CMD_REF){
			MSG_UAV msg;
			msg.cmd = CMD_REF;
			for(int i=0;i<12;++i){
				msg.parameters.push_back(uav::getElapsedTime());
			}
			serialCMM.send2UAV(msg);

//			msg.parameters.clear();
//			msg.cmd = CMD_RETURN_MEA;
//			for(int i=0;i<15;++i){
//				msg.parameters.push_back(0);
//			}
//			serialCMM.send2UAV(msg);
		}
		else if(autoCMD==AUTO_CMD_CMD_RECEIVED){
			MSG_UAV msg;
			msg.cmd = 2;
			serialCMM.send2UAV(msg);
		}
		else{

		}

		rosRate.sleep();
	}
}

void* userCommand(void* args){
	//pthread_t userReturn_tid;
	//int user_ret = pthread_create(&userReturn_tid, NULL, userCommandReturn, NULL);

	pthread_t autoCmd_tid;
	int autoCmd = pthread_create(&autoCmd_tid, NULL, autoCommand, NULL);
	cout<<"[INIT] User command ready for input."<<endl;


	// open file for recording gps
	ofstream gpsFile;
	gpsFile.open("WayPoint-gps.txt", ios::out|ios::trunc);
	gpsFile.close();



	ros::Rate rosRate(FREQUENCY);


	while(!FLAG_MISSION_START && !FLAG_EXIT){
		std::string command;
		std::getline(std::cin, command);
		autoCMD = AUTO_CMD_NONE;

		if(command.compare("gps")==0){
			MSG_UAV msg;
			msg.cmd = CMD_REQUEST_GPS;
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("location")==0){
			MSG_UAV msg;
			msg.cmd = CMD_REQUEST_LOCATION;
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("heading")==0){
			MSG_UAV msg;
			msg.cmd = CMD_REQUEST_HEADING;
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("takeoff")==0){
			MSG_UAV msg;
			msg.cmd = CMD_TAKEOFF;
			msg.parameters.push_back(10);
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("land")==0){
			MSG_UAV msg;
			msg.cmd = CMD_LAND;
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("gps-auto")==0){
			autoCMD = AUTO_CMD_GPS;
		}
		else if(command.compare("location-auto")==0){
			autoCMD = AUTO_CMD_LOCATION;
		}
		else if(command.compare("ref-auto")==0){
			autoCMD = AUTO_CMD_REF;
		}
		else if(command.compare("stop-auto")==0){
			autoCMD = AUTO_CMD_NONE;
		}
		else if(command.compare("exit")==0){
			FLAG_EXIT = true;
			break;
		}
		else if(command.compare("mea")==0){
			MSG_UAV msg;
			msg.cmd = CMD_REQUEST_MEA_BEGIN;
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("cmdr")==0){
			MSG_UAV msg;
			msg.cmd = CMD_MISSION_BEGIN_RECEIVED;
			serialCMM.send2UAV(msg);
		}
		else if(command.compare("cmdr-auto")==0){
			autoCMD = AUTO_CMD_CMD_RECEIVED;
		}
		else if(command.compare("gps-r")==0){
			double begin_t = uav::getElapsedTime();
			bool getGPSFlag = false;

			MSG_UAV msg;
			MSG_UAV msg_re;


			while(uav::getElapsedTime()<begin_t+2){
				msg.cmd = CMD_REQUEST_GPS;
				serialCMM.send2UAV(msg);
				usleep(60000);

				msg_re = serialCMM.getUAVMsg();
				if(msg_re.cmd==CMD_RETURN_GPS) {
					getGPSFlag = true;
					break;
				}


				rosRate.sleep();
			}

			if(getGPSFlag!=true){
				cout<<"Error getting GPS coordinates."<<endl;
				continue;
			}


			double lattitude = msg_re.parameters.at(0);
			double longitude = msg_re.parameters.at(1);
			double height = msg_re.parameters.at(2);



			gpsFile.open("WayPoint-gps.txt", ios::out|ios::app);

			if(gpsFile.is_open()){
				gpsFile <<fixed<< setprecision(10)<<lattitude<<"  "<<setprecision(10)<<longitude<<"  "<<setprecision(3)<<height<<endl;
				cout<<fixed<<setprecision(10)<<lattitude<<"  "<<setprecision(10)<<longitude<<"  "<<setprecision(3)<<height<<"    record ok."<<endl;
				cout.unsetf(ios::fixed);
				cout.precision(10);
			} else {
				cout<<"ERROR writing to WayPoint-gps.txt"<<endl;
			}


			gpsFile.close();
		}

		rosRate.sleep();
	}

	//pthread_join(userReturn_tid, NULL);

}
//----------------- for user command --- end
