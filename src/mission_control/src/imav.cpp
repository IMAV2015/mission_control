/*
 * imav.cpp
 *
 *  Created on: Jul 11, 2015
 *      Author: li
 */



#include <sstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <unistd.h>

#include "communication.h"
#include "mission.h"
#include "pathPlanning.h"
#include "logData.h"
#include "missionLogic.h"
#include "uavState.h"

#ifdef Cpp11
#include <chrono>
#endif

#include <pthread.h>
#include <sched.h>

using namespace std;
using namespace uav;



void initialization(){
	// initialize timer
#ifdef Cpp11
	uav::t_start = std::chrono::steady_clock::now();
#else
	uav::t_start = ros::Time::now().toSec();
#endif

	// get current running directory
	GetCurrentDir(cDir, sizeof(cDir));
	cout<<"current directory: "<<cDir<<endl<<endl;


	// init various nodeCMM
	serialCMM.init(ros2serial);
	visualCMM.init(imav2visual);
	waterCMM.init(imav2water);


	uav::readPara();

	stopWater();

}


int main(int argc, char **argv){

	//initialize ros
	ros::init(argc, argv, "imav_node");
	ros::NodeHandle imav_node;
	ros2serial = imav_node.advertise<serial_uav::MSG_UAV_ROS>("ros2serial", 1000);
	serial2ros = imav_node.subscribe("serial2ros", 1000, &nodeCMM_serial::serial2ros_callback, &serialCMM);

	imav2visual = imav_node.advertise<serial_uav::MSG_UAV_ROS>("imav2visual", 1000);
	visual2imav = imav_node.subscribe("visual2imav", 1000, &nodeCMM_visual::visual2imav_callback, &visualCMM);

	imav2water = imav_node.advertise<std_msgs::UInt16>("Firefighting", 1000);
	water2imav = imav_node.subscribe("Water", 1000, &nodeCMM_water::water2imav_callback, &waterCMM);

	initialization();

	//ROS Spin
	ros::AsyncSpinner spinner(2); // Use 2 threads
	spinner.start();


	usleep(200000);



	// multi-thread
	int status, rr_min_priority, rr_max_priority;
	rr_min_priority = sched_get_priority_min(SCHED_RR);
	rr_max_priority = sched_get_priority_max(SCHED_RR);

	pthread_attr_t thread_attr;
	pthread_attr_init(&thread_attr);
	struct sched_param thread_sched_param;
	status = pthread_attr_setschedpolicy(&thread_attr, SCHED_OTHER);
	pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);

	//---------------------------------- user command thread
	pthread_t user_tid;
	thread_sched_param.__sched_priority = 50;
	pthread_attr_setschedparam(&thread_attr, &thread_sched_param);

	int user_ret = pthread_create(&user_tid, &thread_attr, userCommand, NULL);
	usleep(1000);

	//---------------------------------- uav state thread
	pthread_t state_tid;
	thread_sched_param.__sched_priority = 50;
	pthread_attr_setschedparam(&thread_attr, &thread_sched_param);

	int state_ret = pthread_create(&state_tid, &thread_attr, uavState, NULL);
	usleep(1000);

	//---------------------------------- data logging thread
	pthread_t log_tid;
	thread_sched_param.__sched_priority = 60;
	pthread_attr_setschedparam(&thread_attr, &thread_sched_param);

	int log_ret = pthread_create(&log_tid, &thread_attr, logAll, NULL);
	usleep(1000);

	//---------------------------------- mission control thread
	pthread_t logic_tid;
	thread_sched_param.__sched_priority = 90;
	pthread_attr_setschedparam(&thread_attr, &thread_sched_param);

	int logic_ret = pthread_create(&logic_tid, &thread_attr, logic, NULL);






	pthread_join(user_tid, NULL);
	if(FLAG_EXIT){

	} else {
		pthread_join(logic_tid, NULL);
		pthread_join(log_tid, NULL);
	}


//	ros::Rate rosRate(2*FREQUENCY);
//	while (ros::ok()) {
//		ros::spinOnce();
//		rosRate.sleep();
//	}

	return 0;
}
