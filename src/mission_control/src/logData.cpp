/*
 * logData.cpp
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 */

#include <fstream>
#include <iostream>

#include "logData.h"



void* logAll(void* args){
	cout<<"[INIT] Data logging ready."<<endl;

	ros::Rate rosRate(FREQUENCY);

	time_t t = time(0);   // get time now
	struct tm * now = localtime( & t );

#ifdef Cpp11
	string filename_data = std::to_string(now->tm_year+1900) + string("-") + std::to_string(now->tm_mon+1) + string("-") + std::to_string(now->tm_mday)
			+ string("_") + std::to_string(now->tm_hour) + "-" + std::to_string(now->tm_min) + "-" + std::to_string(now->tm_sec) + string("_data.txt");
	std::ofstream logDataFile;
	logDataFile.open((string)"log/"+filename_data, ios::out | ios::trunc);

	string filename_cmm = std::to_string(now->tm_year+1900) + string("-") + std::to_string(now->tm_mon+1) + string("-") + std::to_string(now->tm_mday)
				+ string("_") + std::to_string(now->tm_hour) + "-" + std::to_string(now->tm_min) + "-" + std::to_string(now->tm_sec) + string("_cmm.txt");
	std::ofstream logCMMFile;
	logCMMFile.open((string)"log/"+filename_cmm, ios::out | ios::trunc);
#else
	string filename_data = tostr(now->tm_year+1900) + string("-") + tostr(now->tm_mon+1) + string("-") + tostr(now->tm_mday)
			+ string("_") + tostr(now->tm_hour) + "-" + tostr(now->tm_min) + "-" + tostr(now->tm_sec) + string("_data.txt");
	std::ofstream logDataFile;
	logDataFile.open(((string)"log/"+filename_data).c_str(), ios::out | ios::trunc);

	string filename_cmm = tostr(now->tm_year+1900) + string("-") + tostr(now->tm_mon+1) + string("-") + tostr(now->tm_mday)
				+ string("_") + tostr(now->tm_hour) + "-" + tostr(now->tm_min) + "-" + tostr(now->tm_sec) + string("_cmm.txt");
	std::ofstream logCMMFile;
	logCMMFile.open(((string)"log/"+filename_cmm).c_str(), ios::out | ios::trunc);
#endif


	while(FLAG_MISSION_COMPLETE!=true){
		logData(logDataFile);
		logCMM(logCMMFile);

		//usleep(floor(1000000.0/FREQUENCY));
		rosRate.sleep();
	}
}

void logData(ofstream &file){

	REFERENCE ref = mission.getReference();
	UAV_STATE mea = state_uav.getUAVState();



	vector<double> ref_pos = pp.getRefPos();
	vector<double> ref_vel = pp.getRefVel();
	vector<double> ref_acc = pp.getRefAcc();


	file << uav::getElapsedTime() << "  ";
	file << ref.x << "  " << ref.y << "  " << ref.z << "  " << ref.c << "  " ;
	file << ref_pos.at(0) << "  " << ref_pos.at(1) << "  " << ref_pos.at(2) << "  " << ref_pos.at(3) << "  " ;
	file << ref_vel.at(0) << "  " << ref_vel.at(1) << "  " << ref_vel.at(2) << "  " << ref_vel.at(3) << "  " ;
	file << ref_acc.at(0) << "  " << ref_acc.at(1) << "  " << ref_acc.at(2) << "  " << ref_acc.at(3) << "  " ;
	file << mea.x_pos << "  " << mea.y_pos << "  " << mea.z_pos << "  "<<mea.c_pos<< "  ";
	file << mea.x_vel << "  " << mea.y_vel << "  " << mea.z_vel << "  "<<mea.c_vel<< "  ";
	file << mea.x_acc << "  " << mea.y_acc << "  " << mea.z_acc << "  "<<mea.c_acc<< "  ";
	file << mea.a_pos << "  " << mea.b_pos << "  " << mea.cc_pos << "  ";
	file << mission.getDelta() << "  ";
	file << endl;

}

void logCMM(ofstream &file){
	// UAV   0 - receive. 1 - send.
	// Visual 2 - receive. 3 - send.

	// log received data.

	MSG_UAV msg = serialCMM.getUAVMsg();
	MSG_UAV msg_visual = visualCMM.getMsg();

	double elapsedTime = uav::getElapsedTime();

	// log received UAV data
	if(sem_trywait(&readUAV_sem)!=-1){
		file << elapsedTime << "  "<<0<<"  ";
		file << msg.cmd << "  ";
		for(int i=0;i<msg.parameters.size();++i){
			file << msg.parameters.at(i) << "  ";
		}
		file<<endl;
	}

	//log received Visual data
	if(sem_trywait(&readVisual_sem)!=-1){
		file << elapsedTime << "  "<<2<<"  ";
		file << msg_visual.cmd << "  ";
		for(int i=0;i<msg_visual.parameters.size();++i){
			file << msg_visual.parameters.at(i) << "  ";
		}
		file<<endl;
	}

	// log sent UAV data.
	if(sem_trywait(&send2UAV_sem)!=-1){
		file << elapsedTime << "  " << 1 << "  ";
		file << serialCMM.getSentUAVMsg().cmd << "  ";
		for (int i = 0; i < serialCMM.getSentUAVMsg().parameters.size(); ++i) {
			file << serialCMM.getSentUAVMsg().parameters.at(i) << "  ";
		}
		file << endl;
	}

	// log sent Visual data
	if(sem_trywait(&send2Visual_sem)!=-1){
		file << elapsedTime << "  " << 3 << "  ";
		file << visualCMM.getSentMsg().cmd << "  ";
		for (int i = 0; i < visualCMM.getSentMsg().parameters.size(); ++i) {
			file << visualCMM.getSentMsg().parameters.at(i) << "  ";
		}
		file << endl;
	}

}
