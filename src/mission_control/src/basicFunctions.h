/*
 * basicFunctions.h
 *
 *  Created on: Jul 12, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_BASICFUNCTIONS_H_
#define MISSION_CONTROL_SRC_BASICFUNCTIONS_H_

//#define Cpp11


#include <ctime>
#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <vector>
#include <math.h>
#include <unistd.h>
#include <cstring>
#include <fstream>
#include <sstream>

#ifdef Cpp11
#include <chrono>
#endif

#include <pthread.h>
#include <semaphore.h>

#include "ros/ros.h"
#include "std_msgs/UInt16.h"


#define FREQUENCY 20
#define CYCLE_TIME 1.0/FREQUENCY



// ------------------------ parameters
extern double takeoff_height;
extern double takeoff_wait;

extern double cmd_auto_freq;

extern double pp_takeoff_maxVel_xy;
extern double pp_takeoff_maxVel_z;
extern double pp_takeoff_maxVel_c;
extern double pp_takeoff_maxAcc_xy;
extern double pp_takeoff_maxAcc_z;
extern double pp_takeoff_maxAcc_c;

extern double pp_land_maxVel_xy;
extern double pp_land_maxVel_z;
extern double pp_land_maxVel_c;
extern double pp_land_maxAcc_xy;
extern double pp_land_maxAcc_z;
extern double pp_land_maxAcc_c;

extern double pp_fly_maxVel_xy;
extern double pp_fly_maxVel_z;
extern double pp_fly_maxVel_c;
extern double pp_fly_maxAcc_xy;
extern double pp_fly_maxAcc_z;
extern double pp_fly_maxAcc_c;

// for height by water sensor
extern double safe_height;
extern double hover_margin;


// for QR code
extern int waypoint_src;

extern double qr_height;
extern double qr_height_2;
extern double qr_heading;
extern double qr_land_lat;
extern double qr_land_lon;

// for visual guidance
extern double visual_guide_max_time;

// ------------------------ parameters end



class Lock {
    pthread_mutex_t &m;
    bool locked;
    int error;
public:
    explicit Lock(pthread_mutex_t & _m) : m(_m) {
        error = pthread_mutex_lock(&m);
        if (error == 0) {
            locked = true;
        } else {
            locked = false;
        }
    }
    ~Lock() {
        if (locked)
            pthread_mutex_unlock(&m);
    }
    bool is_locked() {
        return locked;
    }
};


extern sem_t send2UAV_sem;
extern sem_t readUAV_sem;
extern sem_t send2Visual_sem;
extern sem_t readVisual_sem;


extern bool FLAG_MISSION_COMPLETE;
extern bool FLAG_MISSION_START;
extern bool FLAG_EXIT;

extern ros::Publisher ros2serial;
extern ros::Subscriber serial2ros;

extern ros::Publisher imav2visual;
extern ros::Subscriber visual2imav;

extern ros::Publisher imav2water;
extern ros::Subscriber water2imav;


//#define INPI(angle)		(angle -= floor((angle+M_PI-0.0000000001)/(2*M_PI))*2*M_PI)
#define INPI(angle)		(angle -= floor((angle+M_PI)/(2*M_PI))*2*M_PI)
#define GetCurrentDir getcwd

extern char cDir[FILENAME_MAX];



extern double origin_mea_x;
extern double origin_mea_y;
extern double origin_mea_z;
extern double origin_mea_c;

struct MEASUREMENT {
	double x, y, z, c;
};

struct REFERENCE {
	double x, y, z, c;
	double period;
};

struct UAV_STATE{
	double x_pos;
	double y_pos;
	double z_pos;
	double c_pos;

	double x_vel;
	double y_vel;
	double z_vel;
	double c_vel;

	double x_acc;
	double y_acc;
	double z_acc;
	double c_acc;

	double a_pos;
	double b_pos;
	double cc_pos;
};


namespace uav{


struct WayPnt
{
	double x;
	double y;
	double z;
	double c;
	double period;
	int target;
	int type;
};

//extern clock_t t_start;
//extern long tick_start;
#ifdef Cpp11
extern std::chrono::steady_clock::time_point t_start;
#else
extern double t_start;
#endif

double getElapsedTime();

void printVector(std::vector<double>* src);

int readFromTxt(std::vector<WayPnt>* wayPoints);

int readFromQRCode(std::vector<WayPnt>* wayPoints);

void printWayPoints(std::vector<uav::WayPnt>* wayPoints);

void readPara();

void px4Sound();



}






void initOtherByRelativeMea(double mea_x, double mea_y, double mea_z, double mea_c);




#endif /* MISSION_CONTROL_SRC_BASICFUNCTIONS_H_ */
