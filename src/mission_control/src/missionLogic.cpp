/*
 * missionLogic.cpp
 *
 *  Created on: Jul 19, 2015
 *      Author: li
 */

#include "missionLogic.h"

using namespace std;

MSG_UAV msg_receive;

//Menglu
vector<double> ref_for_print;
UAV_STATE mea_for_print;
REFERENCE tar_for_print;
int c_print=0;

void print_outdoor_debug()
{
	//Menglu
	c_print++;
	if (c_print==40) {
		c_print=0;
		tar_for_print= mission.getReference();
		ref_for_print = pp.getRefPos(); //mission.getReference();
		mea_for_print = state_uav.getUAVState();

		printf("tar: %6.3lf\t%6.3lf\t%6.3lf\t%6.3lf\n", tar_for_print.x, tar_for_print.y, tar_for_print.z, tar_for_print.c);
		printf("ref: %6.3lf\t%6.3lf\t%6.3lf\t%6.3lf\n", ref_for_print.at(0), ref_for_print.at(1), ref_for_print.at(2), ref_for_print.at(3));
		printf("mea: %6.3lf\t%6.3lf\t%6.3lf\t%6.3lf\n", mea_for_print.x_pos, mea_for_print.y_pos, mea_for_print.z_pos, mea_for_print.c_pos);
		//mission.printFlag();
		printf("time: %6.1f\n", uav::getElapsedTime());

		//cout<<"Target      " <<setw(10)<<tar_for_print.x << "  " << tar_for_print.y << "  " << tar_for_print.z << "  " << tar_for_print.c <<endl;
		//cout<<"Reference   " <<setw(10)<<ref_for_print.at(0) << "  " << ref_for_print.at(1) << "  " << ref_for_print.at(2) << "  " << ref_for_print.at(3) <<endl;
		//cout<<"Measurement " <<setw(10)<<mea_for_print.x_pos << "  " << mea_for_print.y_pos << "  " << mea_for_print.z_pos << "  " <<mea_for_print.c_pos <<endl;

		cout<<endl;
	}
}

void* logic(void* args){

	cout<<"[INIT] Mission logic ready."<<endl;

	ros::Rate rosRate(FREQUENCY);


	// -------------------- for debug begin
	cout<<"version 2.1"<<endl;
	uav::px4Sound();
	sleep(2);
	// -------------------- for debug end





	// ------------------- read waypoint from WayPoint.txt
	vector<uav::WayPnt>* wayPoints = new vector<uav::WayPnt>();
	int num_waypoint = 0;
	switch (waypoint_src){
	case 1:{
		MSG_UAV msg;
		while(ros::ok()){
			msg = visualCMM.getMsg();
			if(msg.cmd==CMD_VISUAL_QR && msg.parameters.size()==1 && (int)msg.parameters.at(0)==1){
				cout<<"--[MISSION] got gps from QR code"<<endl;
				break;
			}

			rosRate.sleep();
		}

		num_waypoint = uav::readFromQRCode(wayPoints);
		break;
	}
	case 0:{
		num_waypoint = uav::readFromTxt(wayPoints);
		break;
	}
	default:{
		num_waypoint = uav::readFromTxt(wayPoints);
		break;
	}
	}
	cout << "Number of way points: " << (int)num_waypoint << endl << endl;
	if (num_waypoint < 1){
		cout<<"No given way points."<<endl<<"Mission control exits."<<endl;
		return NULL;
	}

	uav::printWayPoints(wayPoints);
	uav::px4Sound();

	// ------------------- waiting for mission command;
	cout << endl << "---[MISSION] Waiting for command." << endl;
	while (serialCMM.getUAVMsg().cmd != CMD_MISSION_BEGIN) {
		rosRate.sleep();
	}
	FLAG_MISSION_START = true;
	cout << endl << "---[MISSION] Waiting for command confirmation." << endl;
	while (serialCMM.getUAVMsg().cmd != CMD_MISSION_CONFIRM) {
		MSG_UAV mission_received;
		mission_received.cmd = CMD_MISSION_BEGIN_RECEIVED;
		serialCMM.send2UAV(mission_received);
		rosRate.sleep();
	}
	cout << endl << "---[MISSION] command received." << endl;



	// ------------------- initialize missions. convert GPS coordinations into SI.
	vector<MissionDesc> missionSet;
	bool is_missionInit = false;
	while (!is_missionInit) {
		is_missionInit = initializeMissionSet(*wayPoints, missionSet, ros2serial);
		rosRate.sleep();
	}
	cout<<endl<<"---[MISSION] Initialization done."<<endl;
	printMissionSet(missionSet);


	// ------------------- send measurement request
	while (serialCMM.getUAVMsg().cmd != CMD_RETURN_MEA) {
		MSG_UAV mea_req;
		mea_req.cmd = CMD_REQUEST_MEA_BEGIN;
		serialCMM.send2UAV(mea_req);
		rosRate.sleep();
	}
	cout << endl << "---[MISSION] measurement request sent." << endl;
	usleep(300000);    // ensure that measurement is received.
	while(serialCMM.getUAVMsg().cmd!=CMD_RETURN_MEA){
		rosRate.sleep();
	}


	// ------------------- init path planning for take off
	double height = takeoff_height;
	REFERENCE takeoffRef;
	while (pp.is_initialized == false) {
		pp.initializeGPSPathPlanning(state_uav.getUAVState());

		takeoffRef.x = state_uav.getUAVState().x_pos;
		takeoffRef.y = state_uav.getUAVState().y_pos;
		takeoffRef.z = height + origin_mea_z;
		takeoffRef.c = state_uav.getUAVState().c_pos;
		takeoffRef.period = 0;

		rosRate.sleep();
	}


	// ------------------- speed up propellers.
	cout << endl << "---[MISSION] speeding up propellers." << endl;
	pp.resetPathPlanning(PP_GPS);
	double currHeight = state_uav.getUAVState().z_pos;
	for(int i=0;i<6;++i){
		currHeight -= 0.2;
		mission.setReference(takeoffRef, takeoffRef.c);
		mission.setReferenceZ(currHeight);
		mission.setPeroid(1.5);

		mission.target = TargetNone;
		mission.type = MissionTakeOff;
		mission.setBeginTime();

		while(!mission.checkFlag()){
			pp.planningSimple(mission.ref);
			pp.sentReference2UAV(ros2serial);

			#ifdef OUTDOOR_DEBUG_FLAG
				print_outdoor_debug(); //lan menglu
			#endif

			rosRate.sleep();
		}

	}



	// ------------------- takeoff
	cout << endl << "---[MISSION] begin take off. Height: " << height << endl;
	pp.resetPathPlanning(PP_TAKEOFF);
	mission.setReference(takeoffRef, takeoffRef.c);
	mission.target = TargetNone;
	mission.type = MissionTakeOff;
	mission.setBeginTime();

	while(!mission.checkFlag()){
		// path planning for take off
		pp.planningSimple(mission.ref);
		pp.sentReference2UAV(ros2serial);

		#ifdef OUTDOOR_DEBUG_FLAG
			print_outdoor_debug(); //lan menglu
		#endif

		rosRate.sleep();
	}

	cout << endl << "---[MISSION] Take off done." << endl;
	sleep((int)takeoff_wait);

	// ------------------- init gps path planning for gps navigation
	cout << endl << "---[MISSION] mission begin." << endl;
	pp.resetPathPlanning(PP_GPS);
	pp.is_initialized = true;


	// ------------------- begin mission
	for (int missionIndex = 0; missionIndex < missionSet.size(); ++missionIndex) {

		pp.reflexxes_status = 0; //byc

		mission.setMissionFromSet(missionSet, missionIndex);  // setBeginTime is inside.



		cout << "---[MISSION] mission " << missionIndex << " begins" << endl;

		while (!mission.checkFlag()) {

			// path planning for GPS
			if (mission.type == MissionGPS) {
				pp.planningSimple(mission.ref);
				pp.sentReference2UAV(ros2serial);
			}
			// visual guidance with GPS
			else if (mission.type == MissionVisualGuideXY) {
				// x,y reference comes from Vision node.
				vector<double> XYref = visualXY.getVisualGuidanceXY();
				mission.setReferenceXY(XYref.at(0), XYref.at(1));
				pp.planningSimple(mission.ref);
				pp.sentReference2UAV(ros2serial);
			}
			// above water, control height by water sensor and GPS
			else if(mission.type == MissionWaterZ){
				// ref_z is determined by GPS & water sensor. all other references are from the previous mission.
				double ref_z = waterZ.getZByWaterSensor();
				mission.setReferenceZ(ref_z);
				pp.planningSimple(mission.ref);
				pp.sentReference2UAV(ros2serial);
			}
			// indoor mission
			else if (mission.type == MissionIndoor) {

			}
			// other situations
			else {

			}


			// process target.
			processTraget();


			#ifdef OUTDOOR_DEBUG_FLAG
				print_outdoor_debug(); //lan menglu
			#endif

			rosRate.sleep();

		}

	}

	cout << endl << "---[MISSION] mission done." << endl;
	// wait before landing
	sleep(1);


	// ------------------- init path planning for landing
	height = -3;
	pp.resetPathPlanning(PP_LAND);
	REFERENCE landRef;

	while (pp.is_initialized == false) {

		landRef.x = state_uav.getUAVState().x_pos;
		landRef.y = state_uav.getUAVState().y_pos;
		landRef.z = origin_mea_z + height;
		landRef.c = state_uav.getUAVState().c_pos;
		//landRef.c = INPI(mission.ref.c);
		landRef.period = 0;
		pp.is_initialized =true;

		rosRate.sleep();
	}


	// ------------------- landing
	cout << endl << "---[MISSION] begin landing." << endl;
	mission.setReference(landRef, INPI(mission.ref.c));
	mission.target = TargetNone;
	mission.type = MissionGPS;
	mission.setBeginTime();

	while (serialCMM.getUAVMsg().cmd != CMD_LAND_RECEIVED) {
		MSG_UAV msg_landing;
		msg_landing.cmd = CMD_LAND;
		serialCMM.send2UAV(msg_landing);

		rosRate.sleep();
	}

	while(!mission.checkFlag()){
		// path planning for GPS
		pp.planningSimple(mission.ref);
		pp.sentReference2UAV(ros2serial);

		#ifdef OUTDOOR_DEBUG_FLAG
			print_outdoor_debug(); //lan menglu
		#endif

		rosRate.sleep();
	}



	cout << endl << "---[MISSION] Landing slowly." << endl;

	// ------------------- landing slowly
	landRef.z = origin_mea_z + 100;
	pp.resetPathPlanning(PP_LAND_SLOW);
	pp.is_initialized = true;


	mission.setReference(landRef, INPI(mission.ref.c));
	mission.target = TargetNone;
	mission.type = MissionLand;
	mission.setBeginTime();


	while(!mission.checkFlag()){
		// path planning for GPS
		pp.planningSimple(mission.ref);
		pp.sentReference2UAV(ros2serial);

		#ifdef OUTDOOR_DEBUG_FLAG
			print_outdoor_debug(); //lan menglu
		#endif

		rosRate.sleep();
	}



	cout << endl << "---[MISSION] Landing done." << endl;


	FLAG_MISSION_COMPLETE = true;
}
