/*
 * mission.cpp
 *
 *  Created on: Jul 12, 2015
 *      Author: li
 */

#include "mission.h"
//#include "pathPlanning.h"

using namespace std;

bool CurrentMission::checkVisit(){

	REFERENCE reference = getReference();
	MEASUREMENT measuerment = getMeasurement();

	#ifdef REFLEXXES_REACH_FLAG

	if (pp.reflexxes_status == ReflexxesAPI::RML_FINAL_STATE_REACHED)
		return true;
	else return false;

	#else

	bool is_visited = false;

	double WaypntXDiff = reference.x - measuerment.x;
	double WaypntYDiff = reference.y - measuerment.y;
	double WaypntZDiff = reference.z - measuerment.z;
	double WaypntCDiff = reference.c - measuerment.c;
	INPI(WaypntCDiff);

	double WPDiff = pow(WaypntXDiff, 2) + pow(WaypntYDiff, 2);

	switch (type) {
		case MissionLocation:

		case MissionGPS: {
			if ((WPDiff < (1 ^ 2)) && (abs(WaypntZDiff) < 1)
					&& (abs(WaypntCDiff) < (M_PI / 18)))
				is_visited = true;
			else {
				//printf("Distance: %f\n", WPDiff);
				//printf("CDiff: %f\n", abs(WaypntCDiff));
				is_visited = false;
			}
			break;
		}
		case MissionVisualGuideXY: {
			if(visualXY.isMissionVisualGuideXYDone())
				is_visited = true;
			break;
		}
		case MissionIndoor: {
			printf("Warning! Indoor mission not ready.\n");
			is_visited = true;
			break;
		}
		case MissionTakeOff: {
			if (abs(WaypntZDiff) < 1)
				is_visited = true;
			break;
		}
		case MissionLand: {
			if (measuerment.z > 100)
				is_visited = true;
			break;
		}
		case MissionWaterZ: {
			if(waterZ.isMissionWaterZDone())
				is_visited = true;
			break;
		}
		default:
			is_visited = true;
		}

	return is_visited;


	#endif
}

void CurrentMission::setMissionFromSet(vector<MissionDesc> &missionSet, int missionIndex){
	target = missionSet.at(missionIndex).target;
	type = missionSet.at(missionIndex).type;


	if (missionIndex > 0) {
		switch (type){
		case MissionWaterZ:{

			setReference(missionSet.at(missionIndex).ref,
					missionSet.at(missionIndex - 1).ref.c);


			break;
		}
		default:
			setReference(missionSet.at(missionIndex).ref,
					missionSet.at(missionIndex - 1).ref.c);

		}



	} else {
		setReference(missionSet.at(missionIndex).ref, ref.c);    // old c is from take off.
	}


	setBeginTime();
}


bool initializeMissionSet(const vector<uav::WayPnt> src, vector<MissionDesc>& dst, ros::Publisher ros2serial){
	// get measurement from UAV(Pixhawk)
	ros::Rate rosRate(FREQUENCY);

	MSG_UAV msg;
	MSG_UAV msg_receive;
	bool flag_gps = false;
	bool flag_si = false;
	double getGPSTime, getSITime;
	std::cout<<endl<<"---[MISSION] negotiating with UAV for mission initialization."<<endl;

	double mea_latitude = 0;
	double mea_longitude = 0;
	double mea_height = 0;
	int num_satellite = 0;

	double mea_x = 0;
	double mea_y = 0;
	double mea_z = 0;
	double mea_c = 0;

	int counter_thre = 10;

	int gps_counter = 0;
	double mea_latitude_sum = 0;
	double mea_longitude_sum = 0;
	double mea_height_sum = 0;
	while(!flag_gps){
		// request gps coordination.
		msg.cmd = CMD_REQUEST_GPS;
		serialCMM.send2UAV(msg);
		msg_receive = serialCMM.getUAVMsg();
		if(msg_receive.cmd==CMD_RETURN_GPS){
			if(msg_receive.parameters.at(3)>=4 && ((int)msg_receive.parameters.at(0))!=0){
				mea_latitude_sum += msg_receive.parameters.at(0);
				mea_longitude_sum += msg_receive.parameters.at(1);
				mea_height_sum += msg_receive.parameters.at(2);
				num_satellite = (int)msg_receive.parameters.at(3);

				gps_counter += 1;
			}
		}

		if(gps_counter>=counter_thre){
			mea_latitude = mea_latitude_sum / gps_counter;
			mea_longitude = mea_longitude_sum / gps_counter;
			mea_height = mea_height_sum / gps_counter;

			flag_gps = true;
			getGPSTime = uav::getElapsedTime();
			break;
		}

		rosRate.sleep();
	}

	int mea_counter = 0;
	double mea_x_sum = 0;
	double mea_y_sum = 0;
	double mea_z_sum = 0;
	double mea_c_sum = 0;
	while(!flag_si){
		// request UAV location in SI. i.e. x,y,z,c
		msg.cmd = CMD_REQUEST_LOCATION;
		serialCMM.send2UAV(msg);
		msg_receive = serialCMM.getUAVMsg();
		if(msg_receive.cmd==CMD_RETURN_LOCATION){
			mea_x_sum += msg_receive.parameters.at(0);
			mea_y_sum += msg_receive.parameters.at(1);
			mea_z_sum += msg_receive.parameters.at(2);
			mea_c_sum += msg_receive.parameters.at(3);

			mea_counter += 1;
		}

		if(mea_counter>=counter_thre){
			mea_x = mea_x_sum / mea_counter;
			mea_y = mea_y_sum / mea_counter;
			mea_z = mea_z_sum / mea_counter;
			mea_c = mea_c_sum / mea_counter;

			initOtherByRelativeMea(mea_x, mea_y, mea_z, mea_c);

			flag_si = true;
			getSITime = uav::getElapsedTime();
			break;
		}

		rosRate.sleep();
	}

	if(getSITime - getGPSTime > 5){
		cout<<"Pixhawk is too slow for initialization."<<endl;
		return false;
	}

	for(int i=0;i<src.size();++i){
		double ref_latitude = src.at(i).x;
		double ref_longitude = src.at(i).y;
		double ref_height = src.at(i).z;
		double ref_cInDegree = src.at(i).c;

		int type = src.at(i).type;
		MissionDesc temp;
		temp.type = type;
		temp.target = src.at(i).target;

		// get the reference in SI.
		if(type==MissionGPS){
			double xdiff = (ref_latitude - mea_latitude) / 180 * M_PI * 6.3781e6;
			double ydiff = (ref_longitude - mea_longitude) / 180 * M_PI * 6.3781e6 * cos(ref_latitude / 180 * M_PI);

			temp.ref.x = mea_x + xdiff;
			temp.ref.y = mea_y + ydiff;
			temp.ref.z = mea_z + ref_height;
			temp.ref.c = ref_cInDegree * M_PI / 180.0;
			temp.ref.period = src.at(i).period;

			dst.push_back(temp);
		}
		else if(type==MissionVisualGuideXY){
			double xdiff = (ref_latitude - mea_latitude) / 180 * M_PI * 6.3781e6;
			double ydiff = (ref_longitude - mea_longitude) / 180 * M_PI * 6.3781e6 * cos(ref_latitude / 180 * M_PI);

			temp.ref.x = mea_x + xdiff;
			temp.ref.y = mea_y + ydiff;
			temp.ref.z = mea_z + ref_height;
			temp.ref.c = ref_cInDegree * M_PI / 180.0;
			temp.ref.period = src.at(i).period;

			dst.push_back(temp);
		}
		else if(type==MissionWaterZ){
			temp.ref.x = dst.at(i-1).ref.x;
			temp.ref.y = dst.at(i-1).ref.y;
			temp.ref.z = dst.at(i-1).ref.z;
			temp.ref.c = dst.at(i-1).ref.c;
			temp.ref.period = src.at(i).period;

			dst.push_back(temp);
		}
		else if(type==MissionIndoor){
			temp.ref.x = ref_latitude + mea_x;
			temp.ref.y = ref_longitude + mea_y;
			temp.ref.z = mea_z + ref_height;
			temp.ref.c = ref_cInDegree * M_PI / 180.0;
			temp.ref.period = src.at(i).period;

			dst.push_back(temp);
		}
		else if(type==MissionLocation){
			temp.ref.x = mea_x + ref_latitude;
			temp.ref.y = mea_y + ref_longitude;
			temp.ref.z = mea_z + ref_height;
			temp.ref.c = ref_cInDegree * M_PI / 180.0;
			temp.ref.period = src.at(i).period;

			dst.push_back(temp);
		}
		else{
			cout<<"!!!!!!!!!!WARNING!!!!!!!!!!! unknown way point type."<<endl;
		}
	}

	return true;
}

void printMissionSet(vector<MissionDesc>& src){
	cout<<endl<<"initialized mission description:"<<endl;
	cout<<"    x   "<<"    y   "<<"    z   "<<"    c   "<<" period "<<" target "<<"  type  "<<endl;
	for(int i=0;i<src.size();++i){
		cout.precision(5);
		cout<<setw(6)<<src.at(i).ref.x<<"  "<<setw(6)<<src.at(i).ref.y<<"  "<<setw(6)<<src.at(i).ref.z<<"  "<<setw(6)<<src.at(i).ref.c<<"  "<<setw(6)<<src.at(i).ref.period
				<<"  "<<setw(6)<<src.at(i).target<<"  "<<setw(6)<<src.at(i).type<<endl;
	}
	cout<<endl;
}

REFERENCE r;
CurrentMission mission(r, MissionGPS, TargetNone);



int counter_release = 0;
// ----------------- target process
void processTraget(){
	int target = mission.target;
		switch (target) {
		case TargetNone:{
			mission.setAdditionFlag(true);
			break;
		}
		case TargetGetWater:{
			if(mission.checkVisit() && mission.checkPeriod() && mission.is_targetBegin==false && mission.is_targetFinish==false){
				// position reached, begin processing target
				mission.is_targetBegin = true;
				mission.targetBeginTime = uav::getElapsedTime();
				getWater();
			}
			// decide if target is finished.
			if(uav::getElapsedTime()>mission.targetBeginTime+3 && mission.is_targetBegin==true && mission.is_targetFinish==false){
				mission.is_targetFinish = true;
				mission.targetFinishTime = uav::getElapsedTime();
				mission.setAdditionFlag(true);
			}
			break;
		}
		case TargetDropWater:{
			if (mission.checkVisit() && mission.checkPeriod() && mission.is_targetBegin==false && mission.is_targetFinish==false) {
				// position reached, begin processing target
				mission.is_targetBegin = true;
				mission.targetBeginTime = uav::getElapsedTime();
				dropWater();
			}
			// decide if target is finished.
			if (uav::getElapsedTime()>mission.targetBeginTime+5 && mission.is_targetBegin==true && mission.is_targetFinish==false) {
				mission.is_targetFinish = true;
				mission.targetFinishTime = uav::getElapsedTime();
				mission.setAdditionFlag(true);
			}
			break;
		}
		case TargetRelease:{
			if( mission.checkVisit() && mission.checkPeriod() ){
				if(mission.is_targetFinish==false) releasePayload();

				if(mission.is_targetBegin==false){
					mission.targetBeginTime = uav::getElapsedTime();
					mission.is_targetBegin = true;
				}

				vector<MSG_UAV> buffer = serialCMM.getUAVMsgBuffer();
				for(int i=0;i<buffer.size();++i){
					if(buffer.at(i).cmd==CMD_RELEASE_PAYLOAD_RECEIVED) mission.is_targetFinish = true;
				}

				if(mission.is_targetFinish==true && uav::getElapsedTime()-mission.targetBeginTime>=3){
					mission.setAdditionFlag(true);
					mission.targetFinishTime = uav::getElapsedTime();
				}
			}
			break;
		}
		case TargetSaveImgBegin:{
			if (mission.checkVisit() && mission.checkPeriod() && mission.is_targetBegin==false && mission.is_targetFinish==false) {
				// position reached, begin processing target
				mission.is_targetBegin = true;
				mission.targetBeginTime = uav::getElapsedTime();
			}
			// process target
			if(mission.is_targetBegin==true && mission.is_targetFinish==false) saveImgBegin();
			// decide if target is finished.
			if (uav::getElapsedTime()>mission.targetBeginTime+0.1 && mission.is_targetBegin==true && mission.is_targetFinish==false) {
				mission.is_targetFinish = true;
				mission.targetFinishTime = uav::getElapsedTime();
				mission.setAdditionFlag(true);
			}
			break;
		}
		case TargetSaveImgEnd:{
			if (mission.checkVisit() && mission.checkPeriod() && mission.is_targetBegin==false && mission.is_targetFinish==false) {
				// position reached, begin processing target
				mission.is_targetBegin = true;
				mission.targetBeginTime = uav::getElapsedTime();
				saveImgEnd();
			}
			// process target
			if(mission.is_targetBegin==true && mission.is_targetFinish==false) saveImgEnd();
			// decide if target is finished.
			if (uav::getElapsedTime()>mission.targetBeginTime+0.1 && mission.is_targetBegin==true && mission.is_targetFinish==false) {
				mission.is_targetFinish = true;
				mission.targetFinishTime = uav::getElapsedTime();
				mission.setAdditionFlag(true);
			}
			break;
		}
		}

}

void getWater(){
	std_msgs::UInt16 msg;
	msg.data = 1;
	//imav2water.publish(msg);
	waterCMM.send2Water(msg);
}

void dropWater(){
	std_msgs::UInt16 msg;
	msg.data = 2;
	//imav2water.publish(msg);
	waterCMM.send2Water(msg);
}

void stopWater(){
	std_msgs::UInt16 msg;
	msg.data = 0;
	//imav2water.publish(msg);
	waterCMM.send2Water(msg);
}

void releasePayload(){
	MSG_UAV msg;
	msg.cmd = CMD_RELEASE_PAYLOAD;
	serialCMM.send2UAV(msg);

	counter_release+=1;
}

void saveImgBegin(){
	MSG_UAV msg;
	msg.cmd = CMD_VISUAL_STITCH_CTL;
	msg.parameters.push_back(1);
	visualCMM.send2Visual(msg);
}
void saveImgEnd(){
	MSG_UAV msg;
	msg.cmd = CMD_VISUAL_STITCH_CTL;
	msg.parameters.push_back(0);
	visualCMM.send2Visual(msg);

	MSG_UAV msg_2;
	msg_2.cmd = CMD_VISUAL_STITCH_CTL;
	msg_2.parameters.push_back(100);
	visualCMM.send2Visual(msg_2);
}




// ----------------- external mission
// MissionVisualGuideXY
UAV_VisualXY visualXY;

bool UAV_VisualXY::isMissionVisualGuideXYDone(){
	if(is_begin==true && uav::getElapsedTime()-beginTime >= visual_guide_max_time) is_end = true;

	if(visualCMM.getMsg().cmd==CMD_VISUAL_GUIDANCE_CTL && (int)visualCMM.getMsg().parameters.at(0) == 100){
			is_end = true;
	}

	if(is_end==true){
		MSG_UAV msg;
		msg.cmd = CMD_VISUAL_GUIDANCE_CTL;
		msg.parameters.push_back(0);
		visualCMM.send2Visual(msg);
	}

	return is_end;
}
vector<double> UAV_VisualXY::getVisualGuidanceXY(){
	if(is_begin==false){
		is_begin=true;
		beginTime = uav::getElapsedTime();

		MSG_UAV msg;
		msg.cmd = CMD_VISUAL_GUIDANCE_CTL;
		msg.parameters.push_back(1);
		visualCMM.send2Visual(msg);
	}

	UAV_STATE currMea = state_uav.getUAVState();
	vector<double> currPPRef = pp.getRefPos();

	double newX = currPPRef.at(0);
	double newY = currPPRef.at(1);

	MSG_UAV visualMsg = visualCMM.getMsg();
	if(visualMsg.cmd==CMD_VISUAL_GUIDANCE){

		double relativeX = visualMsg.parameters.at(0);
		double relativeY = visualMsg.parameters.at(1);
		//double visualZ = visualMsg.parameters.at(2);


		double theta_z = M_PI - state_uav.getUAVState().c_pos;
		double x1 = relativeX * cos(theta_z) + relativeY * sin(theta_z);
		double y1 = -1 * relativeX * sin(theta_z) + relativeY * cos(theta_z);

		double theta_y = state_uav.getUAVState().b_pos;
		double theta_x = -1 * state_uav.getUAVState().a_pos;


		newX = currPPRef.at(0) + x1;
		newY = currPPRef.at(1) + y1;

		// avoid error
		if(abs(relativeX)>10 || abs(relativeY)>10){
			newX = mission.getReference().x;
			newY = mission.getReference().y;
		}

		visualCMM.clearReceivedMsg();
	} else {
		newX = mission.getReference().x;
		newY = mission.getReference().y;
	}




	vector<double> dst;
	dst.push_back(newX);
	dst.push_back(newY);

	return dst;
}


// MissionWaterZ
UAV_Water waterZ;
bool UAV_Water::isMissionWaterZDone(){
	if(waterCMM.getMsg().data==1) return true;
	else if(pp.getRefPos().at(2)>=safe_height-0.1) return true;
	else return false;
}
double UAV_Water::getZByWaterSensor(){
	double ref_z;

	if(isMissionWaterZDone()){
		if(uav_on_water == DESCENDING){
			uav_on_water = HOVERING;

			if(pp.getRefPos().at(2)>=safe_height-0.1){
				dynamicZ = safe_height;
			} else {
				//dynamicZ = state_uav.getUAVState().z_pos+hover_margin;
				dynamicZ = pp.getRefPos().at(2) + hover_margin;
			}


			inWaterTimeBegin = uav::getElapsedTime();
		}
		ref_z = dynamicZ;
	} else {
		ref_z = safe_height;
		uav_on_water = DESCENDING;
	}

	if(ref_z>=safe_height) ref_z = safe_height;

	return ref_z;
}
