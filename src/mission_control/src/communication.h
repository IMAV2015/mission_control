/*
 * communation.h
 *
 *  Created on: Jul 11, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_COMMUNICATION_H_
#define MISSION_CONTROL_SRC_COMMUNICATION_H_

#include <vector>
#include <iostream>
#include <cstdlib>
#include <iterator>
#include <fstream>
#include <iomanip>

#include "basicFunctions.h"
#include "serial_uav/MSG_UAV_ROS.h"
#include "ros/ros.h"


// command with UAV(Pixhawk)-----------
#define CMD_REQUEST_GPS 51
#define CMD_RETURN_GPS 52 // followed by 10000*latitude, 10000*longitude, height, numberOfSatellite

#define CMD_REQUEST_HEADING 61
#define CMD_RETURN_HEADING 62 // followed by heading(in radian)

#define CMD_REQUEST_LOCATION 71
#define CMD_RETURN_LOCATION 72 // followed by x, y, z, c(in radian)

#define CMD_MISSION_BEGIN 1
#define CMD_MISSION_BEGIN_RECEIVED 2
#define CMD_MISSION_CONFIRM 3

#define CMD_TAKEOFF 10  // followed by height (1 double)
#define CMD_TAKEOFF_RECEIVED 11
#define CMD_TAKEOFF_OK 12

#define CMD_LAND 250   // no parameter needed.
#define CMD_LAND_RECEIVED 251

#define CMD_REF 88    // followed by ref_x, ref_y, ref_z, ref_c,   ref_vx, ref_vy, ref_vz, ref_vc,   ref_ax, ref_ay, ref_az, ref_ac (12 double)

#define CMD_REQUEST_MEA_BEGIN 21
#define CMD_RETURN_MEA 22    // followed by mea_x, mea_y, mea_z, mea_c,   mea_vx, mea_vy, mea_vz, mea_vc,   mea_ax, mea_ay, mea_az, mea_ac,  mea_a, mea_b, mea_cc (15 double)
#define CMD_REQUEST_MEA_STOP 28
#define CMD_MEA 25   // followed by mea_x, mea_y, mea_z, mea_c,   mea_vx, mea_vy, mea_vz, mea_vc,   mea_ax, mea_ay, mea_az, mea_ac (12 double)


#define CMD_PX4_SOUND   90
#define CMD_PX4_SOUND_RECEIVED  91

#define CMD_RELEASE_PAYLOAD 100
#define CMD_RELEASE_PAYLOAD_RECEIVED 101



// command with UAV(Pixhawk)-----------


// command with Visual Guidance-----------

#define CMD_VISUAL_GUIDANCE 215  // followed by relative_x, relative_y, z
#define CMD_VISUAL_GUIDANCE_CTL  214  // 1 = begin guidance, 0 = end guidance,     100 above target.


#define CMD_VISUAL_STITCH_CTL   210   // 1 = begin image save, 0 = end image save, 100 = begin stitch


#define CMD_VISUAL_QR   218  //   0 = no gps, 1 = got gps


// command with Visual Guidance-----------



#define Receive_Buffer_Length    5



struct MSG_UAV{
	int cmd;
	std::vector<double> parameters;
};


class nodeCMM_serial{
private:
	MSG_UAV receive;
	MSG_UAV receive_auto_clear;

	std::vector<MSG_UAV> receive_history;

	MSG_UAV sent;
	bool is_read_safe;

	ros::Publisher ros2serial;
	pthread_mutex_t readUAVMsg_mutex;

public:
	nodeCMM_serial(){
		is_read_safe = true;
		pthread_mutex_init (&readUAVMsg_mutex,NULL);

		MSG_UAV zero;
		zero.cmd = 0;
		for(int i=0;i<Receive_Buffer_Length;++i){
			receive_history.push_back(zero);
		}
	}

	~nodeCMM_serial(){
		pthread_mutex_destroy (&readUAVMsg_mutex);
	}

	void init(ros::Publisher &pub_in){
		ros2serial = pub_in;
	}

	// for node that communicate between serial and ros
	void send2UAV(MSG_UAV msg);
	void serial2ros_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg);
	MSG_UAV getUAVMsg();
	MSG_UAV getUAVMsgOnce();
	std::vector<MSG_UAV> getUAVMsgBuffer();
	void clearReceivedMsg();
	MSG_UAV getSentUAVMsg();



};

extern nodeCMM_serial serialCMM;



class nodeCMM_visual{
private:
	MSG_UAV receive;
	MSG_UAV sent;

	ros::Publisher imav2visual;
	pthread_mutex_t readVisualMsg_mutex;

public:
	nodeCMM_visual(){
		pthread_mutex_init (&readVisualMsg_mutex,NULL);
	}
	~nodeCMM_visual(){
		pthread_mutex_destroy (&readVisualMsg_mutex);
	}

	void init(ros::Publisher &pub_in){
		imav2visual = pub_in;
	}
	void send2Visual(MSG_UAV msg);
	void visual2imav_callback(const serial_uav::MSG_UAV_ROS::ConstPtr& ros_msg);
	MSG_UAV getMsg();
	MSG_UAV getMsgOnce();
	void clearReceivedMsg();
	MSG_UAV getSentMsg();

	void printMsg(MSG_UAV msg);

};

extern nodeCMM_visual visualCMM;



class nodeCMM_water{
private:
	std_msgs::UInt16 receive;
	std_msgs::UInt16 sent;

	ros::Publisher imav2water;

public:
	nodeCMM_water(){}
	~nodeCMM_water(){}

	void init(ros::Publisher &pub_in){
		imav2water = pub_in;
	}
	void send2Water(std_msgs::UInt16 msg);
	void water2imav_callback(const std_msgs::UInt16::ConstPtr& ros_msg);
	std_msgs::UInt16 getMsg();
};
extern nodeCMM_water waterCMM;



#define AUTO_CMD_NONE 0
#define AUTO_CMD_GPS 1
#define AUTO_CMD_LOCATION 2
#define AUTO_CMD_REF 3
#define AUTO_CMD_CMD_RECEIVED 4

extern int autoCMD;

void* userCommandReturn(void* args);

void* autoCommand(void* args);

void* userCommand(void* args);




#endif /* MISSION_CONTROL_SRC_COMMUNICATION_H_ */
