/*
 * logData.h
 *
 *  Created on: Jul 16, 2015
 *      Author: li
 */

#ifndef MISSION_CONTROL_SRC_LOGDATA_H_
#define MISSION_CONTROL_SRC_LOGDATA_H_

#include "mission.h"
#include "basicFunctions.h"
#include "communication.h"
#include "pathPlanning.h"
#include "uavState.h"

template <typename T> string tostr(const T& t) {
   ostringstream os;
   os<<t;
   return os.str();
}


void* logAll(void* args);

void logData(ofstream &file);

void logCMM(ofstream &file);



#endif /* MISSION_CONTROL_SRC_LOGDATA_H_ */
