# CMake generated Testfile for 
# Source directory: /home/li/catkin_ws/src
# Build directory: /home/li/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(firefighting_ros)
SUBDIRS(save_img_ros)
SUBDIRS(serial_uav)
SUBDIRS(mission_control)
SUBDIRS(test_serial)
