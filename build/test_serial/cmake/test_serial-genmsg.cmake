# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "test_serial: 2 messages, 0 services")

set(MSG_I_FLAGS "-Itest_serial:/home/li/catkin_ws/src/test_serial/msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg")

# Find all generators

add_custom_target(test_serial_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/li/catkin_ws/src/test_serial/msg/toUAV.msg" NAME_WE)
add_custom_target(_test_serial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "test_serial" "/home/li/catkin_ws/src/test_serial/msg/toUAV.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/li/catkin_ws/src/test_serial/msg/fromUAV.msg" NAME_WE)
add_custom_target(_test_serial_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "test_serial" "/home/li/catkin_ws/src/test_serial/msg/fromUAV.msg" "std_msgs/Header"
)

#
#  langs = 
#


