
(cl:in-package :asdf)

(defsystem "serial_uav-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "fromUAV" :depends-on ("_package_fromUAV"))
    (:file "_package_fromUAV" :depends-on ("_package"))
    (:file "MSG_UAV_ROS" :depends-on ("_package_MSG_UAV_ROS"))
    (:file "_package_MSG_UAV_ROS" :depends-on ("_package"))
  ))