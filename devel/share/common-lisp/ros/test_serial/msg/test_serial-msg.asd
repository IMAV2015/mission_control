
(cl:in-package :asdf)

(defsystem "test_serial-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "toUAV" :depends-on ("_package_toUAV"))
    (:file "_package_toUAV" :depends-on ("_package"))
    (:file "fromUAV" :depends-on ("_package_fromUAV"))
    (:file "_package_fromUAV" :depends-on ("_package"))
  ))